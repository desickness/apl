


enum {
    COLOR_RESET = 0,
    COLOR_RED   = 31,
    COLOR_GREEN = 32,
};


static bool disable_colors = false;

void text_red(const char *str) {
    if (disable_colors) {
        printf("%s", str);
    } else {
	    printf("\x1B[%dm%s\x1B[%dm", COLOR_RED, str, COLOR_RESET);
    }
}

void text_green(const char *str) {
    if (disable_colors) {
        printf("%s", str);
    } else {
	    printf("\x1B[%dm%s\x1B[%dm", COLOR_GREEN, str, COLOR_RESET);
    }
}