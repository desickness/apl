

void interp_init() {
    interp = empty_interp;
    interp.sym_table.top = 0;
}

void interp_str(const char *str) {

    Lexer l = {0};

    lexer_init(&l, str);
    parser_init(&interp.parser, &l, "interp_str");

    Ast **program = parse_program(&interp.parser);
    if (!program) {
        return;
    }

    For (program) {
        Value *val = eval_decl(program[i]);
    }
}

void interp_file(const char *filename) {

    char *input = file_read(filename);
    if (!input) {
        printf("could not find test file: '%s'\n", filename);
        interp.error = true;
        return;
    }
    Lexer l = {0};

    lexer_init(&l, input);
    parser_init(&interp.parser, &l, filename);

    Ast **program = parse_program(&interp.parser);
    if (!program) {
        return;
    }

    For (program) {
        Value *val = eval_decl(program[i]);
        if (val->kind == VALUE_ERROR) {
            value_print(val);
            printf("\n");
            return;
        }
    }
}

void interp_parse_str(const char *str) {
    Lexer l = {0};

    lexer_init(&l, str);
    parser_init(&interp.parser, &l, "interp_parse_str");

    Ast **program = parse_program(&interp.parser);
}

void interp_parse_file(const char *filename) {

    char *input = file_read(filename);
    if (!input) {
        printf("error: could not find file: '%s'\n", filename);
        interp.error = true;
        return;
    }
    Lexer l = {0};

    lexer_init(&l, input);
    parser_init(&interp.parser, &l, filename);

    Ast **program = parse_program(&interp.parser);
}

bool interp_error() {
    return interp.error || interp.parser.errors != 0;
}