
//
// Parser
//

typedef struct {
    Lexer *l;
    Token current;
    Token previous;

    bool error_mode;
    bool silent_mode;
    int  errors;
    
    const char *filename;

} Parser;


Ast *parse_operand(Parser *p);

Ast *parse_expr(Parser *p);

Ast *parse_stmt(Parser *p);

Ast *parse_decl(Parser *p);

void parser_next(Parser *p);

void parser_next(Parser *p) {
    p->previous = p->current;
    p->current = lexer_next_tok(p->l);
}

void parser_skip_newlines(Parser *p) {
    while (p->current.token_type == TOKEN_NEWLINE) {
        parser_next(p);
    }
}

void report_parser_error(Parser *p, const char *fmt, ...) {
    
    if (p->error_mode) {
        return;
    }
    p->error_mode = true;
    p->errors++;
    if (p->silent_mode) {
        return;
    }
    
    int i = 0;
    const char *file_position_start = p->l->file_position;
    for (;;) {
        if (*file_position_start == '\n' || *file_position_start == '\0') {
            break;
        }
        i++;
        file_position_start++;
    }
    Source_Position pos = p->current.pos;
    
    printf("\n");
    text_red("error");
    printf(": ");
    
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);

    printf("\n");
    printf(" --> %s:%d:%d\n", p->filename, pos.line, pos.column);
    printf("   |\n");
    printf("%-2d |  %.*s\n", pos.line, i, p->l->file_position);
    printf("   |");
    printf(" %*c", pos.column, ' ');
    printf("^\n");
    printf("\n");
}


void parser_init(Parser *p, Lexer *l, const char *filename) {
    p->l = l;
    p->current = lexer_next_tok(p->l);
    p->filename = filename;
    
    parser_skip_newlines(p);
}


bool parser_is_prim(Parser *p) {
    return p->current.token_type == TOKEN_INT
        || p->current.token_type == TOKEN_NUMBER
        || p->current.token_type == TOKEN_SYMBOL
        || p->current.token_type == TOKEN_LBRACKET;
}

bool parser_is_operator(Parser *p) {
    return lexer_is_buildin_op(p->current.token_type);
}

bool parser_is_token(Parser *p, Token_Type t) {
    if (p->current.token_type == t) {
        return true;
    }
    return false;
}

bool parser_is_func(Parser *p) {
    if (lexer_is_buildin_func(p->current.token_type)) {
        return true;
    } else if (parser_is_token(p, TOKEN_CALL)) {
        return true;
    } else {
        return false;
    }
}

bool parser_match(Parser *p, Token_Type t) {
    if (p->current.token_type == t) {
        parser_next(p);
        return true;
    }
    return false;
}


Ast *parse_primitive(Parser *p) {
    Ast *prim = NULL;
    switch(p->current.token_type) {
        case TOKEN_INT:    prim = ast_int(p->current.int_val);    break;
        case TOKEN_NUMBER: prim = ast_num(p->current.num);        break;
        case TOKEN_SYMBOL: prim = ast_symbol(p->current.literal); break;
        default: {
            report_parser_error(p, "unexpected token '%s'", token_type_names[p->current.token_type]);
            return NULL;
        }
    }
    parser_next(p);
    return prim;
}

Ast *parse_array(Parser *p) {
    Ast **arr = NULL;
    while (parser_is_prim(p)) {
        Ast *prim = parse_primitive(p);
        array_push(arr, prim);
    }
    if (!parser_match(p, TOKEN_RBRACKET)) {
        report_parser_error(p, "expected closing ']'");
        return NULL;
    }

    return ast_array(arr);
}

//
//  Operand =
//     | Int
//     | Number
//     | Symbol
//     | Array [(Int|Number|Symbol)+]
//
Ast *parse_operand(Parser *p) {
    if (parser_match(p, TOKEN_LBRACKET)) {
        return parse_array(p);
    } else  {
        return parse_primitive(p);
    }
}

Ast *parse_func(Parser *p) {
    Ast *func = NULL;
    if (parser_is_token(p, TOKEN_CALL)) {
        func = ast_symbol(p->current.literal);
        parser_next(p);
    } else if (parser_is_func(p)) {
        func = ast_builtin(p->current.token_type);
        parser_next(p);
    }
    
    return func;
}


Ast *parse_unary(Parser *p) {
    Ast *func = parse_func(p);
    if (!func) {
        report_parser_error(p, "expected unary function");
        return NULL;
    }
    
    char *op  = NULL;

    
    if (parser_is_operator(p)) {
        op = p->current.literal;
        parser_next(p);
    }

    Ast *expr = parse_expr(p);
    if (!expr) {
        report_parser_error(p, "missing right hand-side of unary expressions");
        return NULL;
    }

    return ast_expr_unary(func, op, expr);
}


//
//  Expr =
//     | Operand
//     | Unary    (Buildin|Operator) Expr
//     | Binary   Expr Buildin Expr
//     | Call     Expr? 
//
Ast *parse_expr(Parser *p) {
    if (parser_is_func(p)) {
       return parse_unary(p);
    }
    
    Ast *expr_left  = parse_operand(p);
    if (!expr_left) {
       return NULL;
    }
    
    if (parser_is_token(p, TOKEN_ASSIGNMENT)
        || parser_is_token(p, TOKEN_ARG)
        || parser_is_token(p, TOKEN_NEWLINE)
        || parser_is_token(p, TOKEN_RBRACE)
        || parser_is_token(p, TOKEN_EOF)) {
        
        return expr_left;
    }
    
    Ast *func = parse_func(p);
    if (!func) {
        report_parser_error(p, "expected binary function");
        return NULL;
    }
    
    Ast *expr_right = parse_expr(p);
    if (!expr_right) {
       report_parser_error(p, "missing right hand-side of binary expressions");
       return NULL;
    }
    
    return ast_expr_binary(func, expr_left, expr_right);
}

//
//  Statement =
//      | Expr
//      | Print Expr
//
Ast *parse_stmt(Parser *p)  {

    bool is_print = false;
    if (parser_is_token(p, TOKEN_PRINT)) {
        is_print = true;
        parser_next(p);
    } 
    
    Ast *expr = parse_expr(p);
    if (!expr) {
        return NULL;
    }
    
    if (is_print) {
        return ast_stmt_print(expr);
    } else {
        return ast_stmt_expr(expr);
    }
}



Ast *parse_block(Parser *p) {
    Ast **decls = NULL;
    if (!parser_match(p, TOKEN_LBRACE)) {
        report_parser_error(p, "expected '{'");
        return NULL;
    }
    parser_skip_newlines(p);
    while (!parser_match(p, TOKEN_RBRACE) && !parser_is_token(p, TOKEN_EOF)) {
        p->error_mode = false;
        Ast *d = parse_decl(p);
        if (d == NULL) {
            break;
        }
        if (p->error_mode) {
            continue;
        }
        array_push(decls, d);
        parser_skip_newlines(p);
    }
            
    if (decls == NULL) {
        report_parser_error(p, "expected one or more declarations");
        return NULL;
    }
            
    if (p->previous.token_type != TOKEN_RBRACE) {
        report_parser_error(p, "missing closing '}'");
        return NULL;
    }
            
    if (p->error_mode) {
        return NULL;
    }
    
    return ast_block(decls);
}


//
//  Decl = 
//     | Func 'fn' Arg? FuncName Arg? '<-' Block 
//     | Var   Symbol '<-' Expr
//     | Statement
//
//
Ast *parse_decl(Parser *p) {
    
    if (parser_match(p, TOKEN_FUNC)) {
        
        Ast *arg_left  = NULL;
        Ast *name      = NULL;
        Ast *arg_right = NULL;
        
        int count = 0;
        while (!parser_match(p, TOKEN_ASSIGNMENT)) {
            
            if (count > 2) {
                 report_parser_error(p, "too many arguments for function or missing assingment '<-'");
                 return NULL;
            }
            
            switch (p->current.token_type) {
                case TOKEN_CALL: {
                    Ast* call = parse_func(p);
                    if (!name) {
                        name = call;
                    } else {
                        report_parser_error(p, "function name already provided");
                        return NULL;
                    }                
                    break;
                }
                case TOKEN_SYMBOL: {
                    if (count == 0) {
                        arg_left = parse_operand(p);
                    } else if (count > 0) {
                        arg_right =  parse_operand(p);
                    }
                    break;
                }
                default: {
                    report_parser_error(p, "expected function name or arguments");
                    return NULL;
                }
            }
            
            count++;
        }
        
        Ast *block = parse_block(p);
        if (!block) {
            return NULL;
        }
        
        return ast_decl_func(name, arg_left, arg_right, block);
    }
    
    Ast *stmt = parse_stmt(p);
    if (!stmt) {
        return NULL;
    }
    
    if (stmt->kind == AST_STMT_EXPR && stmt->expr_stmt.expr->kind == AST_SYMBOL) {
        if (parser_match(p, TOKEN_ASSIGNMENT)) {
            Ast *expr = parse_expr(p);
            if (!expr) {
                report_parser_error(p, "expected expression");
                return NULL;
            }
            return ast_decl_var(stmt->expr_stmt.expr, expr);
        }
    }
    
    return ast_decl_stmt(stmt);    
}

void parser_print_current_token(Parser *p) {
    Source_Position pos = p->current.pos;
    printf("line: %d, column: %d, token: %s, literal: %s\n", pos.line, pos.column, token_type_names[p->current.token_type], p->current.literal);
}

//
// Program = Decl*
//
Ast **parse_program(Parser *p) {
    Ast **program = NULL;
    Ast *decl = parse_decl(p);
    if (!decl) {
        assert(p->error_mode);
        return NULL;
    }
    array_push(program, decl);
    while (true) {
        parser_skip_newlines(p);
        
        if (parser_is_token(p, TOKEN_EOF)) {
            p->error_mode = true;
            break;
        } 
        if (parser_is_token(p, TOKEN_ILLIGAL)) {
            p->error_mode = true;
            break;
        } 
        
        decl = parse_decl(p);
        if (!decl) {
            assert(p->error_mode);
            return NULL;
        }
        array_push(program, decl);
    }
    
    return program;
}




