
#
# Example primitive expressions
#

#
# ints
#

x <- 3 + 2
y <- 123123
r <- y + x

#
# floating point numbers 
#

f <- 1.231
g <- 23132.131
h <- 0.

# arrays

arr   <- [1 2 3 4 5]
mixed <- [3.0 2 2.0]


#
# Simple programs
#

pi <- 3.14159265359

circle_radius <- 5.0
circle_size   <- pi * circle_radius * 2.0
circle_area   <- pi * circle_radius * circle_radius

