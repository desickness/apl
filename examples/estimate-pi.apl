
#
# Estimating PI using 'Dart throwing' 
# Monte Carlo simulation
#
fn $estimate_pi n <- {
    c <- n rho 1.0
    i <- +/ c
   
    x <- n ? 1.0
    y <- n ? 1.0


    x_p <- x - c
    y_p <- y - c

    x_pp <- x_p*x_p
    y_pp <- y_p*y_p
    

    dist   <- x_pp + y_pp
    m      <- n rho 1.000000
    mins   <- dist max m
    mins_p <- mins = m

    sum <- +/ mins_p
    avg <- sum \ i
    res <- avg * 4.0
    
    res
}

print $estimate_pi 100000

