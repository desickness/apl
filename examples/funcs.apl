
#
# Function examples 
#


#
# Unary functions
#

fn $array_sum arr <- {
    +/ arr
}

fn $array_avg arr <- {
    count <- arr_len_func
    sum   <- +/ arr
    sum / count 
}

fn $double x <- { x + x }
fn $square x <- { x * x }

#
# Binary functions
#

fn a $sum b <- {
    a + b
}

# local functions 

fn x $calu y <- {
    fn $doub x <- { x + x }
    fn $squa x <- { x * x }
	
    sq <- $square y
    do <- $double x
   
    result <- sq + do

    result
}    

d <- $double 3
s <- $square 3

cal <- 10 $calu 10

print d
print s
print cal
