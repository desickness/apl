
#
# Simple programs
#

pi <- 3.14159265359

circle_radius <- 5.0
circle_size   <- pi * circle_radius * 2.0
circle_area   <- pi * circle_radius * circle_radius


print circle_radius
print circle_size
print circle_area
