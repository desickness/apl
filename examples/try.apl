

#
# Store 10,000 dice throws
# and show the frequency of all six
# possibilities
#
throws <- ? 10000 rho 6
one    <- +/ 1 = throws
two    <- +/ 2 = throws
three  <- +/ 3 = throws
four   <- +/ 4 = throws
five   <- +/ 5 = throws
six    <- +/ 6 = throws

result <- [one two three four five six]

print result





fn $avg arr <- {
    sum <- +/ arr
    n   <- rho arr
    sum \ n
}

print $avg iota 10