## An APL like programming language

The goal of this project is to learn more about C programming, creating a programming language from scratch, and in general learning more about low level programming.
Everything will be written from scratch in C, meaning no libraries,aside from the C standard library.

### Topics to explore

1. Lexer and parser for a small subset of APL.
2. Writing a x86_64 backend for machine code generation and compiling to a binary executable.
3. Custom memory allocation.
4. Job system for concurrent processing of source files.
5. Fuzz testing in addition to standard test methods.
6. Working with C on multiple platforms (Mac, Windows and Linux).

---
### Code examples

**NOTE**: In APL expressions associate to the right, meaning the following expression
```r
3 * 3 + 4 = 21
```
will be evaluated
```r
3 * (3 + 4) = 3 * 7 = 21
```
and **NOT**
```
(3 * 3) + 4 = 9 + 4 = 13
```

The following code snippets shows some examples of primitive expressions and function declarations.

```r
x <- 3
y <- 123123
r <- y + x
```

```r
pi <- 3.14159265359

circle_radius <- 5.0
circle_size   <- pi * circle_radius * 2.0
circle_area   <- pi * circle_radius * circle_radius
```

#### Using builtin functions

```r
# Sum an array
arr <- [1 2 3 4 5 6 7 8 9 10]
sum <- +/ arr

print sum # 55
```

```r
# Store 10,000 dice throws
throws <- ? 10000 rho 6
```


#### User defined functions

```r
fn $array_sum arr <- {
	+/ arr
}

fn a $sum b <- {
	a + b
}
```



### Compiling the program

#### Windows

Download and install the newest version of Visual Studio, such as the community edition, with the C++ features installed.
Open the "Developer Command Prompt for Visual Studio" by searching for it in the Windows start menu. The Developer Command Prompt has all the needed environment setup, in particular the C/C++ compiler **cl**.

Now just run ``` build.bat ``` within the Developer Command Prompt for Visual Studio.

#### MacOs / Linux

Have any C99 compliant compiler such **gcc** or **clang** installed, and make sure the **cc** alias has been set for a given installed C compiler.

Now just run ``` ./build.sh ``` from the terminal.

### Running the program

When compiled, the resulting executable is placed in a the generated **build** folder. Run the program using ```./build/apl ```on Mac/Linux and ``` build\apl.exe ``` on Windows.

#### Run tests

Run the test using **test** as an argument to the program.


#### TODOS

* Add more builtin apl functions and operators
* Implemented a non-trivial numeric program using the langauge.

