@echo off

if not exist "build" mkdir build
set opts=-FC -GR- -EHa- -nologo -Zi
set code=%cd%
pushd build
cl %opts% %code%\apl.c -Feapl
popd
