//
//  AST described below using a the tagged-union style idiom simular to
//  the discriminated union style found in most functional languages.
//
//  Each node type is represented with a tag using an enum, with a corresponding
//  union case in the Ast struct.
//
//  Each kind has a constructor to ease creation, and has the benefit of
//  enabling a Domain Specific Language style API such as the following example:
//
//      Ast *add = ast_expr_binary(TOKEN_PLUS, ast_int(10), ast_int(123));
//      Ast *mul = ast_expr_binary(TOKEN_ASTERISK, ast_num(10.0), ast_num(14.0));
//
//
//  Program = Decl*
//
//  Decl = 
//     | Func 'fn' Arg? FuncName Arg? '<-' Block 
//     | Var   Symbol '<-' Expr
//     | Statement
//
//  FuncName = '$'Symbol
//  Arg      = Symbol
//  Block    = '{' Decl* '}'
//
//  Statement =
//     | Expr
//     | Print Expr
//
//  Expr =
//     | Operand
//     | Unary    (Buildin|Operator) Expr
//     | Binary   Expr Buildin Expr
//     | Call     Expr? 
//
//  Buildin =
//     | + | - | * | .....
//
//  Opeartor =
//     | Buildin '/'
//
//  Operand =
//     | Int
//     | Number
//     | Symbol
//     | Array [(Int|Number|Symbol)+]
//  
//   Int    = Digit+ 
//   Number = Digit+ ( "." Digit+ )? 
//   Symbol = Alpha ( Aplha | Digit )*
//   Alpha  = 'a' ... 'z' | 'A' ... 'Z' | '_'
//   Digit  = '0' ... '9'
//
//

typedef struct Ast Ast;
typedef enum Ast_Kind Ast_Kind;

enum Ast_Kind {
    AST_INT,
    AST_NUMBER,
    AST_SYMBOL,
    AST_BUILTIN,
    AST_ARRAY,
    
    AST_EXPR_OPERAND,
    AST_EXPR_UNARY,
    AST_EXPR_BINARY,
    AST_EXPR_CALL,
    
    AST_BLOCK,    
    
    AST_STMT_EXPR,
    AST_STMT_PRINT,
    
    AST_DECL_FUNC,
    AST_DECL_VAR,
    AST_DECL_STMT,
};

static const char *ast_kind_str[] = {
    [AST_INT]         = "int",
    [AST_NUMBER]      = "num",
    [AST_SYMBOL]      = "ident",
    [AST_BUILTIN]     = "builtin",
    [AST_ARRAY]       = "array",
    
    [AST_EXPR_UNARY]  = "unary",
    [AST_EXPR_BINARY] = "binary",
    
    [AST_BLOCK]       = "block",
    
    [AST_STMT_EXPR]   = "expr_stmt",
    [AST_STMT_PRINT]  = "print",
    
    [AST_DECL_FUNC]   = "func",
    [AST_DECL_VAR]    = "var",
    [AST_DECL_STMT]   = "stmt",
};


struct Ast {
    Ast_Kind kind;
    union {
        int    integer;
        double number;
        char   *symbol;
        
        Token_Type builtin;
        Ast    **arr;
        struct {
            Ast *arg_left;
            Ast *arg_right;
            Ast *func;
        } call;
        struct {
            Ast *expr;
            Ast *func;
            const char *op;
        } unary;
        struct {
            Ast *expr_left;
            Ast *expr_right;
            Ast *func;
        } binary;
        struct {
            Ast *expr;    
        } print;
        struct {
            Ast *expr;
        } expr_stmt;
        struct {
            Ast *arg_left;
            Ast *arg_right;
            Ast *func_name;
            Ast *block;
        } func;
        struct {
            Ast *symbol;
            Ast *expr;
        } var;
        struct {
            Ast **decls;    
        } block;
        struct {
            Ast *stmt;
        } stmt;
    };
};


Ast *ast_alloc(Ast_Kind kind) {
    Ast *ast = xmalloc(sizeof(Ast));
    ast->kind = kind;
    
    return ast;
}

Ast *ast_int(int i) {
    Ast *ast = ast_alloc(AST_INT);
    ast->integer = i;
    
    return ast;
}

Ast *ast_num(double num) {
    Ast *ast = ast_alloc(AST_NUMBER);
    ast->number = num;
    
    return ast;
}

Ast *ast_array(Ast **arr) {
    assert(arr);
    
    Ast *ast = ast_alloc(AST_ARRAY);
    ast->arr = arr;
    
    return ast;
}

Ast *ast_builtin(Token_Type builtin) {
    Ast *ast = ast_alloc(AST_BUILTIN);
    
    ast->builtin = builtin;
    
    return ast;
}

Ast *ast_symbol(const char *symbol) {
    Ast *ast = ast_alloc(AST_SYMBOL);
    int len = strlen(symbol);
    ast->symbol = xmalloc(len + 1);
    strcpy(ast->symbol, symbol);
    ast->symbol[len] = '\0';
    
    return ast;
}

Ast *ast_expr_call(Ast *func, Ast *arg_left, Ast *arg_right) {
    assert(func);
    assert(arg_left);
    assert(arg_right);
    
    Ast *ast = ast_alloc(AST_EXPR_CALL);
    
    ast->call.func      = func;
    ast->call.arg_left  = arg_left;
    ast->call.arg_right = arg_right;
    
    return ast;
}

Ast *ast_expr_unary(Ast *func, const char *op, Ast *expr) {
    assert(func);
    assert(expr);
    
    Ast *ast = ast_alloc(AST_EXPR_UNARY);
    
    ast->unary.func = func;
    ast->unary.op   = op;
    ast->unary.expr = expr;
    
    return ast;
}

Ast *ast_expr_binary(Ast *func, Ast *left, Ast *right) {
    assert(left);
    assert(right);
    
    Ast *expr = ast_alloc(AST_EXPR_BINARY);
    
    expr->binary.func       = func;
    expr->binary.expr_left  = left;
    expr->binary.expr_right = right;
    
    return expr;
}

Ast *ast_block(Ast **decls) {
    assert(decls);
    
    Ast *ast = ast_alloc(AST_BLOCK);
    
    ast->block.decls = decls;
    
    return ast;
}

Ast *ast_stmt_expr(Ast *expr) {
    assert(expr);
    
    Ast *stmt = ast_alloc(AST_STMT_EXPR);
    
    stmt->expr_stmt.expr = expr;
    
    return stmt;
}

Ast *ast_stmt_print(Ast *expr) {
    Ast *stmt = ast_alloc(AST_STMT_PRINT);
    
    stmt->print.expr = expr;
    
    return stmt;
}



Ast *ast_decl_func(Ast *symbol, Ast *arg_left, Ast *arg_right, Ast *block) {
    Ast *ast = ast_alloc(AST_DECL_FUNC);
    
    ast->func.func_name = symbol;
    ast->func.arg_left  = arg_left;
    ast->func.arg_right = arg_right;
    ast->func.block     = block;
    
    return ast;
}


Ast *ast_decl_var(Ast *symbol, Ast *expr) {
    Ast *stmt = ast_alloc(AST_DECL_VAR);
    
    stmt->var.symbol = symbol;
    stmt->var.expr   = expr;
    
    return stmt;
}


Ast *ast_decl_stmt(Ast *stmt) {
    assert(stmt);
    
    Ast *ast = ast_alloc(AST_DECL_STMT);
    
    ast->stmt.stmt = stmt;
    
    return ast;
}


static int ast_print_indent_curr = 0;
static const int ast_print_indent_size = 2;

void ast_print_indent_increase() {
    ast_print_indent_curr += ast_print_indent_size;
}

void ast_print_indent_decrease() {
    if (ast_print_indent_curr == 0) return;
    ast_print_indent_curr -= ast_print_indent_size;
}

void ast_print_indent() {
    printf("%*c", ast_print_indent_curr, ' ');
}

void ast_print_func(Ast *func) {
    if (func->kind == AST_BUILTIN) {
        printf("%s ", token_type_names[func->builtin]);
    } else {
        printf("%s ", func->symbol);
    }
}

void ast_print(Ast *ast) {
    switch (ast->kind) {
        case AST_INT: {
            printf("%d", ast->integer);
            break;
        }
        case AST_NUMBER: {
            printf("%f", ast->number);
            break;
        }
        case AST_ARRAY: {
            printf("[");
            for (int i = 0; i < array_len(ast->arr); i++) {
                if (i != 0) {
                    printf(" ");
                }
                ast_print(ast->arr[i]);
            }
            printf("]");
            break;
        }
        case AST_SYMBOL: {
            printf("%s", ast->symbol);
            break;
        }
        case AST_BUILTIN: {
            printf("%s", token_type_names[ast->builtin]);
        }
        case AST_EXPR_UNARY: {
            printf("(");
            if (!ast->unary.op) {
                ast_print_func(ast->unary.func);
            } else {
                printf("unary ");
                ast_print_func(ast->unary.func);
                printf(" %s ", ast->unary.op);
            }
            ast_print(ast->unary.expr);
            printf(")");
            break;
        }
        case AST_EXPR_BINARY: {
            printf("(");
            printf("binary ");
            ast_print_func(ast->binary.func);
            ast_print(ast->binary.expr_left);
            printf(" ");
            ast_print(ast->binary.expr_right);
            printf(")");
            break;
        }
        case AST_EXPR_CALL: {
            printf("(");
            ast_print_func(ast->binary.func);
            ast_print(ast->binary.expr_left);
            printf(" ");
            ast_print(ast->binary.expr_right);
            printf(")");
            break;
        }
        case AST_BLOCK: {
            ast_print_indent();
            printf("(block");
            ast_print_indent_increase();
            for (int i = 0; i < array_len(ast->block.decls); i++) {
                printf("\n");
                ast_print_indent();
                ast_print(ast->block.decls[i]);
            }
            ast_print_indent_decrease();
            ast_print_indent_decrease();
            printf(")");
            break;
        }
        case AST_STMT_PRINT: {
            printf("(print ");
            ast_print(ast->print.expr);
            printf(")");
            break;
        }
        case AST_STMT_EXPR: {
            ast_print(ast->expr_stmt.expr);
            break;
        }
        case AST_DECL_STMT: {
            ast_print(ast->stmt.stmt);
            break;
        }
        case AST_DECL_VAR: {
            printf("(");
            printf("var %s : ", ast->var.symbol->symbol);
            ast_print(ast->var.expr);
            printf(")");
            break;
        }
        case AST_DECL_FUNC: {
            
            printf("(");
            printf("func %s [", ast->func.func_name->symbol);
            if(ast->func.arg_left) {
                printf("%s", ast->func.arg_left->symbol);
            }
            if (ast->func.arg_left && ast->func.arg_right) {
                printf(" ");
            }
            if(ast->func.arg_right) {
                printf("%s", ast->func.arg_right->symbol);
            }
            printf("]\n");
            ast_print(ast->func.block);
            ast_print_indent_decrease();
            printf(")");
            break;
        }
        default: {
            fatal("ast print: unknown ast kind");
        }
    }
}


void test_ast() {
    printf("[+] AST tests\n");
    Ast *e1 = ast_num(1.0f);
    Ast *e2 = ast_num(2.0f);
    Ast *e3 = ast_num(3.0f);
    Ast *e4 = ast_num(4.0f);
    Ast *e5 = ast_num(5.0f);
    Ast *e6 = ast_num(6.0f);
    Ast *e7 = ast_num(7.0f);
    Ast *e8 = ast_num(8.0f);
    Ast *e9 = ast_num(9.0f);
    Ast **asts = NULL;
    
    array_push(asts, e1);
    array_push(asts, e2);
    array_push(asts, e3);
    array_push(asts, e4);
    array_push(asts, e5);
    array_push(asts, e6);
    array_push(asts, e7);
    array_push(asts, e8);
    array_push(asts, e9);
    
    printf("array\n");
    Ast *arr = ast_array(asts);
    ast_print(arr);
    printf("\n\n");
    
   
    printf("unary\n");
    Ast *unary = ast_expr_unary(ast_builtin(TOKEN_QUESTION), "/", ast_int(100));
    ast_print(unary);
    printf("\n\n");
    
    printf("binary\n");
    Ast *binary = ast_expr_binary(ast_builtin(TOKEN_PLUS), ast_int(2), ast_int(2));
    ast_print(binary);
    printf("\n\n");
    
    Ast **args = NULL;
    Ast **stmts = NULL;
    Ast *arg1 = ast_symbol("x");
    Ast *arg2 = ast_symbol("y");
    Ast *body = ast_expr_binary(ast_builtin(TOKEN_ASTERISK), arg1, arg2);
    array_push(args, arg1);
    array_push(stmts, body);
    
    Ast* var = ast_decl_var(ast_symbol("val"), ast_num(3.0));
    printf("var\n");
    ast_print(var);
    printf("\n\n");
    
    Ast* var_x = ast_decl_var(ast_symbol("x"), ast_num(3.0));
    Ast* var_y = ast_decl_var(ast_symbol("y"), ast_num(3.0));
    Ast* expr_decl  = ast_decl_stmt(ast_stmt_expr(ast_symbol("y")));
    Ast **vars = NULL;
    array_push(vars, var_x);
    array_push(vars, var_y);
    array_push(vars, expr_decl);
    Ast* block = ast_block(vars);
    printf("block\n");
    ast_print(block);
    printf("\n\n");
    
    printf("function\n");
    Ast *func = ast_decl_func(ast_symbol("calc"), ast_symbol("a"), ast_symbol("b"), block);
    ast_print(func);
    printf("\n\n");
    
    printf("print\n");
    Ast *print = ast_stmt_print(var_x);
    ast_print(print);
    printf("\n\n");
    
    printf("expr_stmt\n");
    Ast *stmt = ast_decl_stmt(binary);
    ast_print(stmt);
    printf("\n\n");
    
}
