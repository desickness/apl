
typedef bool (*Test_Func)();

typedef struct {
    const char *name;
    Test_Func func;
    
    // formatting
    int name_len;
} Test_Case;


//
// testing that the essential lexing is working at expected
//
bool test_lexer() {
    
    int passed = 0;
    int failed = 0;
    
    struct {
        char *token;
        Token_Type token_type;
    } test_cases[] = {
        
        { "15", TOKEN_INT },
        { "1",  TOKEN_INT },
        { "9",  TOKEN_INT },
        { "2",  TOKEN_INT },
        { "0",  TOKEN_INT },
        { "13123123",  TOKEN_INT },
        
        { "12.2",       TOKEN_NUMBER },
        { "0.241",      TOKEN_NUMBER },
        { "9879.21231", TOKEN_NUMBER },
        
        { "<-", TOKEN_ASSIGNMENT },
        { "(",  TOKEN_LPAREN },
        { "/",  TOKEN_FORWARD_SLASH },
        { ")",  TOKEN_RPAREN },
        { "{",  TOKEN_LBRACE },
        { "}",  TOKEN_RBRACE },
        { "[",  TOKEN_LBRACKET },
        { "]",  TOKEN_RBRACKET },
        { ",",  TOKEN_COMMA },
        { "fn", TOKEN_FUNC },
        
        // builtin functions
        { "+",     TOKEN_PLUS },
        { "*",     TOKEN_ASTERISK },
        { "**",    TOKEN_POWER },
        { "-",     TOKEN_MINUS },
        { "iota",  TOKEN_IOTA },
        { "?",     TOKEN_QUESTION},
        { "max",   TOKEN_MAX },
        { "min",   TOKEN_MIN },
        { "map",   TOKEN_MAP },
        { "print", TOKEN_PRINT },
        { "rho",   TOKEN_RHO },
        { "=",     TOKEN_EQUAL },
        // builtin functions end
        
        { "rho_rho",  TOKEN_SYMBOL },
        { "$x",       TOKEN_CALL },
        { "rhorhodfasfsaufyoidfyaoidsfyaoifyud", TOKEN_SYMBOL },
        { "some_long_symbol_with_1_2_3_123_",    TOKEN_SYMBOL },
        { "_leading_underscore_asda_qwe_",       TOKEN_SYMBOL },
        
        { "$with_unserscores_and_number_123",  TOKEN_CALL },
        { "$ cannot_have_space_after_$",       TOKEN_ILLIGAL },
        
        { "\0", TOKEN_EOF },
        
        // end of test cases
        {"", NUM_TOKENS},
    };
    
    for (int i = 0; test_cases[i].token_type != NUM_TOKENS; i++) {
        Lexer l = {0};
        lexer_init(&l, test_cases[i].token);
        Token token = lexer_next_tok(&l);
        if (token.token_type == test_cases[i].token_type) {
            passed++;
        } else {
            failed++;
        }
    }
    
    return failed == 0;
}




bool test_parser() {
    
    interp_init();
    
    int passed = 0;
    int failed = 0;
    
    char *func_1 = "fn $sum <- {\n"
        "             x <- 3 + 4\n"
        "             x\n"
        "           }";
    
    char *func_2 = "fn x $sum y <- {\n"
        "             x <- 3 + 4\n"
        "             x\n"
        "           }";
    
    
    struct {
        char *src;
        Ast_Kind expected_kind;
    } test_cases[] = {
        {"1", AST_DECL_STMT},
        {"[1 1 1 1 1 3]", AST_DECL_STMT},
        {"1.0", AST_DECL_STMT},
        {"x", AST_DECL_STMT},
        {"+ [1 2]", AST_DECL_STMT},
        {"+ 2", AST_DECL_STMT},
        {"x <- 3", AST_DECL_VAR},
        {"x <- [123 12312 3]", AST_DECL_VAR},
        {"lang_name_for_some_value <- [1 2 3 4]", AST_DECL_VAR},
        {func_1, AST_DECL_FUNC},
        {func_2, AST_DECL_FUNC},
        
        {NULL, 0}
    };
    
    for (int i = 0; test_cases[i].src != NULL; i++) {
        Lexer l = {0};
        Parser p = {0};
        
        lexer_init(&l, test_cases[i].src);
        parser_init(&p, &l, "test_parser");
        
        Ast *stmt = parse_decl(&p);
        if (stmt != NULL) {
            passed++;
        } else {
            failed++;
        }
    }
    
    return failed == 0;
}

bool test_eval_base() {

    interp_init();

    int passed = 0;
    int failed = 0;
    
    struct {
        char *src;
        Value *expected;
    } test_cases[] = {
        // base cases
        {"2", value_int(2)},
        {"4", value_int(4)},
        {"1234567        ", value_int(1234567) },
        {"1.0", value_num(1.0) },
        {"241.123   ", value_num(241.123) },
        
        {NULL, NULL},
    };
    for (int i = 0; test_cases[i].src != NULL; i++) {
        Lexer l  = {0};
        Parser p = {0};
        
        lexer_init(&l, test_cases[i].src);
        parser_init(&p, &l, "test_eval_base");
        
        Ast *expr = parse_expr(&p);
        Value *val = eval_expr(expr);
        if (value_equal(val, test_cases[i].expected)) {
            passed++;
        } else {
            failed++;
        }
    }
    
    
    return failed == 0;
}

bool test_eval_unary() {

    interp_init();

    int passed = 0;
    int failed = 0;
    
    struct {
        char *src;
        Value *expected;
    } test_cases[] = {
        {"+ 1", value_int(1) },
        {"- 1", value_int(-1) },
        {"+        23.023", value_num(23.023) },
        
        {NULL, NULL},
    };
    for (int i = 0; test_cases[i].src != NULL; i++) {
        Lexer l  = {0};
        Parser p = {0};
        
        lexer_init(&l, test_cases[i].src);
        parser_init(&p, &l, "test_eval_unary");
        
        Ast *expr = parse_expr(&p);
        Value *val = eval_expr(expr);
        if (value_equal(val, test_cases[i].expected)) {
            passed++;
        } else {
            failed++;
        }
    }
    
    return failed == 0;
}

bool test_eval_unary_operator() {

    interp_init();

    int passed = 0;
    int failed = 0;
    
    struct {
        char *src;
        Value *expected;
    } test_cases[] = {
        {"  -   / [1 2 3]", value_int(0 - 1 - 2 - 3) },
        {"+         / [1 2 3 4]", value_int(1 + 2 + 3 + 4) },
        
        {NULL, NULL},
    };
    for (int i = 0; test_cases[i].src != NULL; i++) {
        Lexer l  = {0};
        Parser p = {0};
        
        lexer_init(&l, test_cases[i].src);
        parser_init(&p, &l, " test_eval_unary_operator");
        
        Ast *expr = parse_expr(&p);
        Value *val = eval_expr(expr);
        if (value_equal(val, test_cases[i].expected)) {
            passed++;
        } else {
            failed++;
        }
    }
    
    
    return failed == 0;
}


bool test_eval_binary() {
    
    interp_init();

    int passed = 0;
    int failed = 0;
    
    struct {
        char *src;
        Value *expected;
    } test_cases[] = {
        {"1 + 1", value_int(2) },
        {"1 + 1 + 1", value_int(3) },
        {"+ 1 + 1", value_int(2) },
        
        {NULL, NULL},
    };
    for (int i = 0; test_cases[i].src != NULL; i++) {
        Lexer l  = {0};
        Parser p = {0};
        
        lexer_init(&l, test_cases[i].src);
        parser_init(&p, &l, "test_eval_binary");
        
        Ast *expr = parse_expr(&p);
        Value *val = eval_expr(expr);
        if (value_equal(val, test_cases[i].expected)) {
            passed++;
        } else {
            failed++;
        }
    }
    
    
    return failed == 0;
}

bool test_parsing_decls() {
    const char *src = "x <- 3 + 3\n"
        "v <- 3 + 3\n"
        "fn $y r <- {\n"
        "\n"
        "\n"
        "\n"
        "\n"
        "\n"
        "\n"
        "\n"
        "r  +r\n"
        "\n"
        "\n"
        "\n"
        "}\n"
        "a <- 3 + 3\n"
        "b <- 3 + 3\n";
        
    interp_init();
    interp.parser.silent_mode = true;
    interp_parse_str(src);    
    
    return !interp_error();
}



bool test_parsing_stmts() {

    const char *src = "x <-1+1\n"
        "fa <- + [1 2 3]\n"
        "adssadasdas<-+ [1 2 3]\n"
        "jkadajlsikd<-+ 1 + 1+1+1+1++1+1+1\n"
        "x <- [3 0 0101] + [11  929 1]\n"
        "asdasdkjahsd <- dsjakjlsadjlasd\n";
        
    interp_init();
    interp.parser.silent_mode = true;
    interp_parse_str(src);
    
    return !interp_error();
}

bool test_eval_variables() {

    const char *src = ""
    "    x <- 3\n"
    "    y <- 3\n"
    "    r <- y + x\n";
    
    interp_init();
    interp.parser.silent_mode = true;
    interp_str(src);
    
    Value *result = sym_table_lookup("r");
    
    return result->integer == 6;
}


bool test_eval_func_binary() {

    const char *src =  
        "fn x $add y <- {\n"
        "    x + y\n"
        "}\n"
        "\n"
        "\n"
        "result <- 10 $add 40\n";
    
    interp_init();
    interp.parser.silent_mode = true;
    interp_str(src);
    
    Value *result = eval_expr(ast_symbol("result"));
    
    return result->integer == 50;
}

bool test_eval_func_unary() {

    const char *src =  
        "fn $sum arr <- {\n"
        "    +/ arr\n"
        "}\n"
        "\n"
        "\n"
        "result <- $sum [1 2 3 4 5 6 7 8 9 10]\n";
        
    interp_init();
    interp.parser.silent_mode = true;
    interp_str(src);
    
    Value *result = sym_table_lookup("result");
    
    return result->integer == 55;
}

bool test_eval_funcs_in_func() {

    const char *src = "\n"
    "fn x $calc y <- {\n"
    "\n"
    "    fn $double b <- { b + b }\n"
    "    fn $square c <- { c * c }\n"
    "\n"
    "    sq <- $square y\n"
    "    do <- $double x\n"
    "\n"
    "    result <- sq + do\n"
    "\n"
    "    result\n"
    "}\n"    
    "\n"
    "result <- 2 $calc 4\n"
    "\n";    
    
    interp_init();
    interp.parser.silent_mode = true;
    interp_str(src);
    
    Value *result = sym_table_lookup("result");
    
    return result->integer == 20;
}


bool test_eval_builtin_funcs() {

    const char *src = "\n"
    "iota_10 <- iota 10\n"
    "mx  <- max/ iota_10\n"
    "mi  <- min/ iota_10\n"
    "fac <- ! 10\n"
    "rnd <- ? 100\n" 
    "len <- rho iota_10\n" 
    "eq  <- 1 = 1\n"
    "pow <- 2**4\n"
    "\n";    
    
    interp_init();
    interp.parser.silent_mode = true;
    interp_str(src);
    
    Value *fac = sym_table_lookup("fac");
    Value *max = sym_table_lookup("mx");
    Value *min = sym_table_lookup("mi");
    Value *rnd = sym_table_lookup("rnd");
    Value *len = sym_table_lookup("len");
    Value *eq  = sym_table_lookup("eq");
    Value *pow = sym_table_lookup("pow");
    
    interp_init();
    
    bool fac_test = fac->integer == 3628800;
    bool max_test = max->integer == 10;
    bool min_test = min->integer == 1;
    bool rnd_test = rnd->integer >= 0 && rnd->integer <= 100;
    bool len_test = len->integer == 10;
    bool eq_test  = eq->integer  == 1;
    bool pow_test = pow->integer == 16;
    
    return fac_test 
        && max_test 
        && min_test
        && rnd_test
        && len_test
        && eq_test
        && pow_test;
}


bool test_parsing_func_in_func() {
    const char *src = "fn a $sum b <- {\n"
        "fn x $double <- { x + x }\n"
        "fn x $square <- { x * x }\n"
        "y <- 3\n"
        "d <- 3\n"
        "\n"
        "\n"
        "res <- y + d\n"
        "\n"
        "\n"
        "\n"
        "res\n"
        "}\n";
        
    interp_init();
    interp.parser.silent_mode = true;
    interp_parse_str(src);
    
    return !interp_error();
}


bool test_sym_table() {
    interp_init();
    Value *x_ident = value_symbol("x");
    Value *x_val = value_int(1);
    
    Value *y_ident = value_symbol("y");
    Value *y_val = value_int(3);
    
    Value *z_ident = value_symbol("z");
    Value *z_val = value_int(5);
    
    sym_table_bind(x_ident, x_val);
    sym_table_bind(y_ident, y_val);
    sym_table_bind(z_ident, z_val);
    
    
    Value *x = sym_table_lookup(x_ident->symbol);
    if (!x) {
        return false;
    } 
    
    Value *y = sym_table_lookup(y_ident->symbol);
    if (!y) {
        return false;
    } 
    
    Value *z = sym_table_lookup(z_ident->symbol);
    if (!z) {
        return false;
    } 
    
    
    return x->integer == 1 && y->integer == 3 && z->integer == 5;
}


bool test_promote_int() {

    int failed = 0;
    
    struct {
        Value *integer;
        Value_Kind target;
        Value *result;
    } test_cases[] = {
        
        // Numbers
        { value_int(1),    VALUE_NUMBER, value_num(1.0) },
        { value_int(1231), VALUE_NUMBER, value_num(1231.0) },
        { value_int(0),    VALUE_NUMBER, value_num(0.0) },
        
        // Arrays 
        { value_int(1), VALUE_ARRAY, unary_iota(value_int(1)) }, 
        // end
        { NULL, 0, NULL }
    };
    
    
    for (int i = 0; test_cases[i].result != NULL; i++) {
        
        Value *res = value_promote(test_cases[i].integer, test_cases[i].target);
        
        if (res->kind != test_cases[i].target) {
            failed++;
        }
        if (!value_equal(res, test_cases[i].result)) {
            failed++;
        }
    }
    
    return failed == 0;
}


static Test_Case test_programs[] = {
    {"../examples/expr.apl"},
    {"../examples/funcs.apl"},
    {"../examples/circle.apl"},
    {"../examples/try.apl"},
    {"../examples/estimate-pi.apl"},
    
    // end
    NULL,
};


void test_programs_all() {
    
    printf("test_parsing_programs\n");
    int longest_name = 30;
    for (int i = 0; test_programs[i].name != NULL; i++) {
        int name_len = strlen(test_programs[i].name);
        test_programs[i].name_len = name_len;
        if (longest_name < name_len) {
            longest_name = name_len;
        }
    }
    
    for (int i = 0; test_programs[i].name != NULL; i++) {
        interp_init();
        interp.parser.silent_mode = true;
        interp_parse_file(test_programs[i].name);
    
        printf("    %s ", test_programs[i].name);
        for (int j = 0; j < longest_name - test_programs[i].name_len + 5; j++) {
            printf(".");
        }
        if (!interp_error()) {
            text_green(" OK");
        } else {
            text_red(" FAILED");
        }
        printf("\n");
    }
    printf("\n");
}

void test_run(const char *name, Test_Case *test_cases){

    printf("%s\n", name);
    int longest_name = 30;
    for (int i = 0; test_cases[i].name != NULL; i++) {
        int name_len = strlen(test_cases[i].name);
        test_cases[i].name_len = name_len;
        if (longest_name < name_len) {
            longest_name = name_len;
        }
    }
    
    for (int i = 0; test_cases[i].name != NULL; i++) {
        printf("    %s ", test_cases[i].name);
        for (int j = 0; j < longest_name - test_cases[i].name_len + 5; j++) {
            printf(".");
        }
        if (test_cases[i].func()) {
            text_green(" OK");
        } else {
            text_red(" FAILED");
        }
        printf("\n");
    }
    printf("\n");
}

void test_all() {

    printf("\n");
    
    //
    // Lexer
    //
    Test_Case lexer_unit_tests[] = {
        { "test_lexer", test_lexer },
        // end
        { NULL, NULL }
    };
    test_run("lexer_unit_tests", lexer_unit_tests);
    
    
    //
    // Parser
    //
    Test_Case parser_unit_tests[] = {
        { "test_parser",              test_parser },
        // end
        { NULL, NULL }
    };
    test_run("parser_unit_tests", parser_unit_tests);
    
    
    //
    // Parser File 
    //
    Test_Case parsing_tests[] = {
        { "test_parsing_decls",        test_parsing_decls },
        { "test_parsing_stmts",        test_parsing_stmts },
        { "test_parsing_func_in_func", test_parsing_func_in_func },
        // end
        { NULL, NULL }
    };
    test_run("parsing_system_tests", parsing_tests);
    
    
    //
    // Promoting values
    //
    Test_Case promote_values_unit_tests[] = {
        { "test_promote_int", test_promote_int },
        
        // end
        { NULL, NULL }
    };
    test_run("promote_values_unit_tests", promote_values_unit_tests);
    
    //
    // Evaluation
    //
    Test_Case eval_unit_tests[] = {
        { "test_eval_base",           test_eval_base},
        { "test_eval_unary",          test_eval_unary},
        { "test_eval_unary_operator", test_eval_unary_operator},
        { "test_eval_binary",         test_eval_binary},
        { "test_eval_variables",      test_eval_variables},
        { "test_eval_func_unary",     test_eval_func_unary},
        { "test_eval_func_binary",    test_eval_func_binary},
        { "test_eval_funcs_in_func",  test_eval_funcs_in_func },
        { "test_eval_builtin_funcs",  test_eval_builtin_funcs },
        // end
        { NULL, NULL }
    };
    test_run("eval_unit_tests", eval_unit_tests);
    
    
    //
    // Symbol Table
    //
    Test_Case sym_table_tests[] = {
        { "test_sym_table", test_sym_table},
        // end
        { NULL, NULL }
    };
    test_run("sym_table_tests", sym_table_tests);
    
    
    //
    // Test programs
    //
    test_programs_all();
}



    