
#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "common.c"

#ifdef _WIN32
    #include "platform_win32.c"
#else 
    #include "platform_unix.c"
#endif

#include "lexer.c"
#include "ast.c"
#include "parser.c"

#include "interp.h"
#include "interp_value.c"
#include "interp_environment.c"
#include "interp_eval.c"
#include "interp.c"

#include "cli.c"
#include "test.c"

static char buffer[2048];

char *readline(char *prompt) {
    fputs(prompt, stdout);
    fgets(buffer, 2048, stdin);
    char *cpy = xmalloc(strlen(buffer) + 1);
    strcpy(cpy, buffer);
    cpy[strlen(cpy) - 1] = '\0';
    return cpy;
}


void repl(bool ast_mode) {
    printf(" apl version 0.1 repl\n");
    printf(" press CTRL-C to exit\n");
    while (true) {
        char *input = readline(">> ");
        
        if (!input) continue;
        Lexer l = {0};
        Parser p = {0};
        
        lexer_init(&l, input);
        parser_init(&p, &l, "repl");
        
        Ast **program = parse_program(&p);
        if (!program) {
            continue;
        }
        
        for (int i = 0; i < array_len(program); i++) {
            if (ast_mode) {
                ast_print(program[i]);
                printf("\n");
            } else {
                Value *v = eval_decl(program[i]);
                if (v->kind != VALUE_ERROR) {
                    value_print(v);
                    printf(" : %s\n", value_kind_str[v->kind]);
                } else {
                    value_print(v);
                    printf("\n");
                }
            }
        }
    }
}

void eval(char *src) {
    Lexer l = {0};
    Parser p = {0};
    
    lexer_init(&l, src);
    parser_init(&p, &l, "eval");
    
    Ast **program = parse_program(&p);
    
    For (program) {
        Value *val = eval_decl(program[i]);
        value_print(val);
        printf("\n");
    }
}

void lex_file(char *filename) {
    char *file = file_read(filename);
    if (!file) {
        printf("could not find file: '%s'\n", filename);
        return;
    }
    
    Lexer l = {0};
    lexer_init(&l, file);
    Token t = {0};
    int line = 0;
    int num  = 0;
    printf("\n");
    printf("Tokens: %s\n", filename);
    while (t.token_type != TOKEN_EOF) {
        t = lexer_next_tok(&l);
        if (t.token_type == TOKEN_NEWLINE) {
            continue;
        }
        if (t.pos.line != line) {
            line = t.pos.line;
            num  = 1;
            printf("\n  %-4d:", line);
            token_print(&t);
        } else if (num > 4) { 
            printf("\n      :");
            token_print(&t);
            num = 1;
        } else {
            printf(", ");
            token_print(&t);
            num++;
        }
    }
    printf("\n\n");
}

void parse_file(const char *filename) {
    char *input = file_read(filename);
    if (!input) {
        printf("could not find file: '%s'\n", filename);
        return;
    }
    
    interp_init();
    Lexer l = {0};
    
    lexer_init(&l, input);
    parser_init(&interp.parser, &l, filename);
    
    Ast **program = parse_program(&interp.parser);
    
    For (program) {
        if (!program[i]) continue;
        ast_print(program[i]);
        printf("\n");
    }
}


void eval_file(char *filename) {
    interp_init();
    interp_file(filename);
}

void tests() {
    test_all();
}

void debug() {
}

static const char *version = "0.0.1";

typedef struct {
    bool repl;
    bool ast_mode;
    bool debug;
    bool test;
    bool help;
    bool version;
    bool no_colors;
    
    char **files;
    char *eval;
    char *run;
    char *lex;
    char *parse;
} Apl_Options;


void options_print(Apl_Options *options) {
    printf("run: %s\n"       , options->run);
    printf("help: %s\n"      , options->help      ? "true" : "false");
    printf("repl: %s\n"      , options->repl      ? "true" : "false");
    printf("ast_mode: %s\n"  , options->ast_mode  ? "true" : "false");
    printf("debug: %s\n"     , options->debug     ? "true" : "false");
    printf("eval: %s\n"      , options->eval);
    printf("no-colors: %s\n" , options->no_colors ? "true" : "false");
    printf("version: %s\n"   , options->version   ? "true" : "false");
    printf("lex: %s\n"       , options->lex);
    printf("pase: %s\n"      , options->parse);
}


int main(int argc, char **argv) {

    Apl_Options options = {0};
    
    flag_program_name = "APL";
    //
    // Commands
    //
    flag_str(&options.run         ,"run"       ,"run APL file");
    flag_bool(&options.help       ,"help"      ,"usage message");
    flag_bool(&options.repl       ,"repl"      ,"start a REPL");
    flag_bool(&options.ast_mode   ,"ast"       ,"print ast information only in repl mode");
    flag_bool(&options.debug      ,"debug"     ,"debug mode");
    flag_bool(&options.test       ,"test"      ,"run tests");
    flag_bool(&options.version    ,"version"   ,"show version");
    flag_str(&options.lex         ,"lex"       ,"lex APL file");
    flag_str(&options.parse       ,"parse"     ,"parse APL file");
    flag_str(&options.eval        ,"eval"      ,"eval expression");
    
    //
    // Settings
    //
    flag_bool(&options.no_colors  ,"no-colors" ,"disable colored output");
	
    
    if (argc == 1) {
        flag_usage();
        return 0;
    }
    
    
    flag_parse(&argc, &argv);
    
    //
    // Handle settings
    //
    if (options.no_colors) {
        disable_colors = true;
    }
    
    //
    // Execute commands
    //
    if (options.run) {
        eval_file(options.run);
    } else if (options.debug) {
        debug();
    } else if (options.test) {
        tests();
    } else if (options.version) {
        printf("%s\n", version);
    } else if (options.repl) {
        repl(options.ast_mode);
    } else if (options.help) {
        flag_usage();
    } else if (options.lex) {
        lex_file(argv[2]);
    } else if (options.parse) {
        parse_file(argv[2]);        
    } else if (options.eval) {
        eval(argv[2]);
    } else {
        printf("\n");
        printf("%s: missing input\n", argv[1]);
        flag_usage();
    }
    
    return 0;
}

    
