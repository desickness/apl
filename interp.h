#ifndef INTERP_H
#define INTERP_H



//
// Interpreter settings
//
#define INTERP_SYM_TABLE_SIZE  4096
#define INTERP_FUNC_TABLE_SIZE 4096


typedef enum {
    VALUE_INT,
    VALUE_NUMBER,
    VALUE_SYMBOL,
    VALUE_ARRAY,
    VALUE_MATRIX,
    VALUE_FUNC,
    VALUE_ERROR,
} Value_Kind;


static const char *value_kind_str[] = {
    [VALUE_INT]    = "int",
    [VALUE_NUMBER] = "num",
    [VALUE_SYMBOL] = "symbol",
    [VALUE_ARRAY]  = "array",
    [VALUE_MATRIX] = "matrix",
    [VALUE_FUNC]   = "func",
    [VALUE_ERROR]  = "error",
};


typedef struct Value Value;
struct Value {
    Value_Kind kind;
    union {
        int    integer;
        double number;
        char   *symbol;
        Value  **arr;
        char   *error;
        struct {
            int rows;
            int cols;
            Value  **mat;
        } matrix;
    };
};


typedef struct {
    Value *symbol;
    Value *val;
} Sym_Table_Entry;

typedef struct {
    Sym_Table_Entry stack[INTERP_SYM_TABLE_SIZE];
    size_t top;
} Sym_Table;


typedef struct Function Function;
typedef Value *(*Unary_Func)(Value *);
typedef Value *(*Binary_Func)(Value *, Value *);

typedef enum {
    FUNC_UNARY,
    FUNC_BINARY
} Func_Kind;

struct Function {
    Func_Kind kind;

    const char *name;

    Ast *arg_left;
    Ast *arg_right;

    Ast **stmts;
};


typedef struct {
    Function *stack[INTERP_FUNC_TABLE_SIZE];
    size_t top;
} Func_Table;

//
// Interpreter
//

typedef struct {

    Parser parser;

    Sym_Table sym_table;
    Func_Table func_table;

    bool error;
} Interp;


static Interp empty_interp = {0};
static Interp interp = {0};

void interp_init();
bool interp_error();
void interp_str(const char *str);
void interp_file(const char *filename);
void interp_parse_str(const char *str);
void interp_parse_file(const char *filename);

//
// Value contructors
//
Value *value_new(Value_Kind kind);
Value *value_int(int i);
Value *value_num(double num);
Value *value_symbol(const char *symbol);
Value *value_arr(Value **arr);
Value *value_matrix(Value **mat, int rows, int cols);
Value *value_error(const char *fmt, ...);
Value *value_func(const char *symbol);
Value *value_copy(Value *val);

//
// Value helpers
//
bool value_kind_equal(Value *a, Value *b); 
bool value_equal(Value *a, Value *b);
void value_print(Value *val);
char *value_str(Value *val);


//
// Value promotion
//
Value *value_promote(Value *value, Value_Kind target);
Value *value_promote_int(Value *i, Value_Kind target);
Value *value_promote_num(Value *num, Value_Kind target);
Value *value_promote_array(Value *arr, Value_Kind target);
Value *value_promote(Value *value, Value_Kind target);
void   value_promote_par(Value *a, Value *b);

Value_Kind value_target(Value *a, Value *b);


//
// Symbol table
//
void sym_table_init();
void sym_table_bind(Value *symbol, Value *val);
void sym_table_exit(int num_args);
void sym_table_print(Sym_Table *st);
Value *sym_table_lookup(const char *name);

//
// Function table for user defined functions
//
Function *function_new(const char *name, Ast *arg_left, Ast *arg_right, Ast **stmts);
Function *func_table_lookup(const char *name);
void func_table_init();
void func_table_bind(Function *func);
void func_scope_enter();
void func_scope_exit();
void func_debug();


//
// Func table for builtin functions
//
// Most builtin functions in APL have a binary and unary version.
// We can simply lookup by token type, since the token type for
// functions are reserved keywords.
//
typedef struct {
    Unary_Func  unary;
    Binary_Func binary;
    
    bool should_promote;
    //
    // Accumulator for reduce
    //
    int acc;

} Func_Entry;


//
// Array helper functions
//
Value *array_binary_func(Binary_Func func, Value *a, Value *b);
Value *array_unary_func(Unary_Func func, Value *a);
Value *array_reduce(Binary_Func func, Value *acc, Value *arr);

//
// Builtin unary functions
//
Value *unary_add(Value *a);
Value *unary_sub(Value *a);
Value *unary_mul(Value *a);
Value *unary_iota(Value *a);
Value *unary_random(Value *a);
Value *unary_fac(Value *a);
Value *unary_rho(Value *a);

//
// Builtin binary functions
//
Value *binary_add(Value *a, Value *b);
Value *binary_sub(Value *a, Value *b);
Value *binary_mul(Value *a, Value *b);
Value *binary_div(Value *a, Value *b);
Value *binary_random(Value *a, Value *b);
Value *binary_min(Value *a, Value *b);
Value *binary_max(Value *a, Value *b);
Value *binary_rho(Value *a, Value *b);
Value *binary_equal(Value *a, Value *b);
Value *binary_power(Value *a, Value *b);


static Func_Entry builtin_func_table[] = {
    [TOKEN_PLUS]       = { unary_add,    binary_add    , true  , 0       },
    [TOKEN_MINUS]      = { unary_sub,    binary_sub    , true  , 0       },
    [TOKEN_ASTERISK]   = { unary_mul,    binary_mul    , true  , 1       },
    [TOKEN_BACK_SLASH] = { NULL,         binary_div    , true  , 1       },
    [TOKEN_IOTA]       = { unary_iota,   NULL          , false , 0       },
    [TOKEN_QUESTION]   = { unary_random, binary_random , false , 1       },
    [TOKEN_BANG]       = { unary_fac,    NULL          , true  , 0       },
    [TOKEN_MIN]        = { NULL,         binary_min    , true  , INT_MAX },
    [TOKEN_MAX]        = { NULL,         binary_max    , true  , INT_MIN },
    [TOKEN_RHO]        = { unary_rho,    binary_rho    , false , 0       },
    [TOKEN_EQUAL]      = { NULL,         binary_equal  , true  , 0       },
    [TOKEN_POWER]      = { NULL,         binary_power  , true  , 1       },
};

Func_Entry func_lookup(Token_Type t);

//
// Evaluation
//
Value *eval_operand(Ast *operand);
Value *eval_unary(Ast *unary);
Value *eval_binary(Ast *binary);
Value *eval_expr(Ast *expr);
Value *eval_stmt(Ast *stmt);
Value *eval_decl(Ast *decl);
Value *eval_function_call(Function *f);
void eval_reset();




#endif /* INTERP_H */
