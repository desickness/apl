
typedef enum {
    TOKEN_ILLIGAL,
    TOKEN_EOF,
    TOKEN_INT,
    TOKEN_NUMBER,
    TOKEN_OPERATOR,
    TOKEN_LPAREN,
    TOKEN_RPAREN,
    TOKEN_LBRACE,
    TOKEN_RBRACE,
    TOKEN_LBRACKET,
    TOKEN_RBRACKET,
    TOKEN_ASSIGNMENT,
    TOKEN_COMMA,
    TOKEN_COLON,
    TOKEN_SEMICOLON,
    TOKEN_NEWLINE,
    TOKEN_SYMBOL,
    TOKEN_ARG,
    TOKEN_DOLLAR,
    TOKEN_FUNC,
    TOKEN_CALL,
    
    // build in functions
    TOKEN_FUNCTIONS_START,
    
    TOKEN_PLUS,
    TOKEN_MINUS,
    TOKEN_ASTERISK,
    TOKEN_BACK_SLASH,
    TOKEN_QUESTION,
    TOKEN_IOTA,
    TOKEN_BANG,
    TOKEN_MIN,
    TOKEN_MAX,
    TOKEN_PRINT,
    TOKEN_RHO,
    TOKEN_EQUAL,
    TOKEN_POWER,
    
    TOKEN_FUNCTIONS_END,
    
    // build in operators
    TOKEN_OPERATORS_START,
    
    TOKEN_FORWARD_SLASH,
    TOKEN_MAP,
    
    TOKEN_OPERATORS_END,
    
    NUM_TOKENS,
    
} Token_Type;


static char *token_type_names[] = {
    [TOKEN_ILLIGAL]       = "ILLIGAL",
    [TOKEN_EOF]           = "EOF",
    [TOKEN_INT]           = "int",
    [TOKEN_NUMBER]        = "num",
    [TOKEN_OPERATOR]      = "operator",
    [TOKEN_LPAREN]        = "(",
    [TOKEN_RPAREN]        = ")",
    [TOKEN_LBRACE]        = "{",
    [TOKEN_RBRACE]        = "}",
    [TOKEN_LBRACKET]      = "[",
    [TOKEN_RBRACKET]      = "]",
    [TOKEN_ASSIGNMENT]    = "<-",
    [TOKEN_COMMA]         = ",",
    [TOKEN_COLON]         = ":",
    [TOKEN_SEMICOLON]     = ";",
    [TOKEN_NEWLINE]       = "\\n",
    [TOKEN_SYMBOL]        = "<symbol>",
    [TOKEN_ARG]           = "<arg>",
    [TOKEN_DOLLAR]        = "$",
    [TOKEN_FUNC]          = "fn",
    [TOKEN_CALL]          = "<call>",
    // build in functions
    [TOKEN_PLUS]          = "+",
    [TOKEN_MINUS]         = "-",
    [TOKEN_ASTERISK]      = "*",
    [TOKEN_QUESTION]      = "?",
    [TOKEN_BANG]          = "!",
    [TOKEN_IOTA]          = "iota",
    [TOKEN_MIN]           = "min",
    [TOKEN_MAX]           = "max",
    [TOKEN_MAP]           = "map",
    [TOKEN_PRINT]         = "print",
    [TOKEN_RHO]           = "rho",
    [TOKEN_EQUAL]         = "=",
    [TOKEN_POWER]         = "**",
    // build in functions end
    
    [TOKEN_BACK_SLASH]    = "\\",
    [TOKEN_FORWARD_SLASH] = "/",
};

typedef struct {
	int line;
    int column;
} Source_Position;

typedef struct {
    char *literal;
    
    const char *start;
    const char *end;
    
    Token_Type token_type;
    
    int int_val;
    
    double num;
    
    Source_Position pos;
} Token;

void token_print(Token *t) {
    if (t->token_type == TOKEN_INT) {
	    printf("['%d' ", t->int_val);
    } else if (t->token_type == TOKEN_NUMBER) {
	    printf("['%f' ", t->num);
    } else if (t->token_type == TOKEN_ARG) {
	    printf("['$%s' ", t->literal);
    } else {
	    printf("['%s' ", t->literal);
    }
    printf("%d:%d]", t->pos.line, t->pos.column);
}

typedef struct {
    const char *name;
    Token_Type token;
} Keyword;


static Keyword keywords[] = {
    { "fn",    TOKEN_FUNC  },
    { "iota",  TOKEN_IOTA  },
    { "min",   TOKEN_MIN   },
    { "max",   TOKEN_MAX   },
    { "map",   TOKEN_MAP   },
    { "print", TOKEN_PRINT },
    { "rho",   TOKEN_RHO   },
    
    // end
    { NULL, TOKEN_ILLIGAL }
};



typedef struct {
    int line_count;
    int curr_line;
    int curr_column;
    
    const char *src;
    const char *file_position;
} Lexer;


void lexer_init(Lexer *l, const char *src) {
    l->line_count    = 0;
    l->src           = src;
    l->file_position = src;
    l->curr_line     = 1;
    l->curr_column   = 1;
}

bool lexer_is_buildin_op(Token_Type t) {
    return t > TOKEN_OPERATORS_START && t < TOKEN_OPERATORS_END;
}

bool lexer_is_buildin_func(Token_Type t) {
    return (t > TOKEN_FUNCTIONS_START && t < TOKEN_FUNCTIONS_END)
        || lexer_is_buildin_op(t);
}


void lexer_skip_whitespace(Lexer *l) {
    
    for (;;) {
        switch (*l->src) {
            case '\t':
            case ' ': {
                while (*l->src == ' '
                       || *l->src == '\t') {
                    l->src++;
                    l->curr_column++;
                    if (*l->src == '\n' || *l->src == '\r') {
                        l->src++;
                        l->curr_line++;
                        l->curr_column = 1;
                        l->file_position = l->src;
                    }
                }
                break;
            }
        }
        
        //
        // Comments
        //
        if (*l->src == '#') {
            while ((*l->src != '\n' && *l->src != '\r') && *l->src != '\0') {
                l->src++;
                l->curr_column++;
            }
            if (*l->src == '\n' || *l->src == '\r') {
                l->src++;
                l->file_position = l->src;
                l->curr_line++;
                l->curr_column = 1;
            }
            continue;
        } else {
            break;
        }
    }
}

Source_Position lexer_current_position(Lexer *l) {
    return (Source_Position){l->curr_line, l->curr_column};
}

void lexer_advance(Lexer *l) {
    l->src++;
    l->curr_column++;
}

Token lexer_next_tok(Lexer *l) {
    
    lexer_skip_whitespace(l);
    
    Token tok      = {0};
    tok.token_type = -1;
    tok.start      = l->src;
    
    switch (*l->src) {
        
        // 
        // Macro for trivial token cases
        //
        #define TOKEN_CASE(token, kind)   case token : {     \
            tok.pos        = lexer_current_position(l);      \
            tok.token_type = kind;                           \
            tok.literal    = token_type_names[kind];         \
            tok.end        = l->src;                         \
            lexer_advance(l);                                \
            break;                                           \
        }
        
        
        TOKEN_CASE('+',  TOKEN_PLUS)
        TOKEN_CASE('-',  TOKEN_MINUS)
        TOKEN_CASE('/',  TOKEN_FORWARD_SLASH)
        TOKEN_CASE('\\', TOKEN_BACK_SLASH)
        TOKEN_CASE('(',  TOKEN_LPAREN)
        TOKEN_CASE(')',  TOKEN_RPAREN)
        TOKEN_CASE('{',  TOKEN_LBRACE)
        TOKEN_CASE('}',  TOKEN_RBRACE)
        TOKEN_CASE('[',  TOKEN_LBRACKET)
        TOKEN_CASE(']',  TOKEN_RBRACKET)
        TOKEN_CASE('?',  TOKEN_QUESTION)
        TOKEN_CASE('!',  TOKEN_BANG)
        TOKEN_CASE(',',  TOKEN_COMMA)
        TOKEN_CASE(':',  TOKEN_COLON)
        TOKEN_CASE(';',  TOKEN_SEMICOLON)
        TOKEN_CASE('=',  TOKEN_EQUAL)
        
        #undef TOKEN_CASE
        
        //
        // newline support for both LF and CRLF
        //
        case '\n' : 
        case '\r' : {
            tok.pos        = lexer_current_position(l);
            tok.token_type = TOKEN_NEWLINE;
            tok.literal    = token_type_names[TOKEN_NEWLINE];
            tok.end        = l->src;
            lexer_advance(l);
            l->curr_column = 1;
            l->curr_line++;
            l->file_position = l->src;
            break;
        }
        case '<': {
            tok.pos = lexer_current_position(l);
            lexer_advance(l);
            if (*l->src == '-') {
                tok.token_type = TOKEN_ASSIGNMENT;
                tok.literal    = token_type_names[TOKEN_ASSIGNMENT];
                tok.end        = l->src;
                lexer_advance(l);
            }
            break;
        }
        case '*': {
            tok.pos = lexer_current_position(l);
            tok.token_type = TOKEN_ASTERISK;
            tok.literal    = token_type_names[TOKEN_ASTERISK];
            tok.end        = l->src;
            lexer_advance(l);
            if (*l->src == '*') {
                tok.token_type = TOKEN_POWER;
                tok.literal    = token_type_names[TOKEN_POWER];
                tok.end        = l->src;
                lexer_advance(l);
            }
            break;
        }
        case '$': {
            tok.pos = lexer_current_position(l);
            lexer_advance(l);
            if (isalpha(*l->src)) {
                while (isalnum(*l->src) || *l->src == '_') {
                    lexer_advance(l);
                }
                tok.end = l->src;
                tok.start++; // discard the $
                int len = tok.end - tok.start;
                
                tok.token_type   = TOKEN_CALL;
                tok.literal      = xmalloc(sizeof(char) * len + 1);
                tok.literal[len] = '\0';

                strncpy(tok.literal, tok.start, len);
                
                break;
            } else {
                tok.token_type = TOKEN_ILLIGAL;
            }
            break;
            
        }
        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j':
        case 'k': case 'l': case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't':
        case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J':
        case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T':
        case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
        case '_': {
            tok.pos = lexer_current_position(l);
            while (isalnum(*l->src) || *l->src == '_') {
                lexer_advance(l);
            }
            tok.end = l->src;
            int len = tok.end - tok.start;
            
            tok.token_type   = TOKEN_SYMBOL;
            tok.literal      = xmalloc(sizeof(char) * len + 1);
            tok.literal[len] = '\0';
            
            strncpy(tok.literal, tok.start, len);
            
            //
            // For now we just lookup the keywords everytime we encounter 
            // a symbol.
            //
            // -fbl 8-10-19
            //
            for (int i = 0; keywords[i].name != NULL; i++) {
                if (str_eq(keywords[i].name, tok.literal)) {
                    tok.token_type = keywords[i].token;
                    break;
                }
            }
            
            
            break;
        }
        // digits
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': {
            tok.pos = lexer_current_position(l);
            int val = 0;
            while (isdigit(*l->src)) {
                val = val * 10 + (*l->src++ - '0');
            }
            if (*l->src == '.') {
                lexer_advance(l);
                while (isdigit(*l->src)) {
                    lexer_advance(l);
                }
                tok.num        = strtod(tok.start, NULL);
                tok.token_type = TOKEN_NUMBER;
                tok.end        = l->src;
            } else {
                tok.int_val    = val;
                tok.token_type = TOKEN_INT;
                tok.end        = l->src;
            }
            break;
        }
        case '\0': {
            tok.pos        = lexer_current_position(l);
            tok.token_type = TOKEN_EOF;
            tok.end        = l->src;
            lexer_advance(l);
            break;
        }
        default: {
            tok.token_type = TOKEN_ILLIGAL;
            break;
        }
    }

    return tok;
}
