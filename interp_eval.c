
//
// Builtin function helper functions
//

Value *array_reduce(Binary_Func func, Value *acc, Value *arr) {
    assert(arr->kind == VALUE_ARRAY && "a must be a array");

    if (acc->kind != arr->arr[0]->kind) {
        value_error("reduce: type mismatch");
    }
    
    acc = value_promote(acc, arr->arr[0]->kind);
    
    For(arr->arr) {
        acc = func(acc, arr->arr[i]);
    }

    return acc;
}


Value *array_binary_func(Binary_Func func, Value *a, Value *b) {
    assert(a->kind == VALUE_ARRAY && "a must be a array");
    assert(b->kind == VALUE_ARRAY && "b must be a array");
    
    Value **arr = NULL;
    
    size_t len_a = array_len(a->arr);
    size_t len_b = array_len(b->arr);
    
    if (len_a == 1) {
        Value *operand = a->arr[0];
        For(b->arr) {
            Value *value = func(operand, b->arr[i]);
            if (value->kind == VALUE_ERROR) {
                return value;
            }
            array_push(arr, value);
        }
        
        return value_arr(arr);
    }
    
    if (len_b == 1) {
        Value *operand = b->arr[0];
        For(a->arr) {
            Value *value = func(a->arr[i], operand);
            if (value->kind == VALUE_ERROR) {
                return value;
            }
            array_push(arr, value);
        }
        
        return value_arr(arr);
    }
    
    if (len_a == len_b) {
        For(a->arr) {
            Value *val_a = a->arr[i];
            Value *val_b = b->arr[i];
            Value *res   = func(val_a, val_b);
            
            if (res->kind == VALUE_ERROR) {
                return res;
            }
            array_push(arr, res);
        }   
    
        return value_arr(arr);
    } else {
        return value_error("cannot apply function to arrays sizes %d, %d", len_a, len_b);
    }
}

Value *array_unary_func(Unary_Func func, Value *a) {
    assert(a->kind == VALUE_ARRAY && "a must be a array");
    
    Value **arr = NULL;
    
    For(a->arr) {
        Value *value = func(a->arr[i]);
        if (value->kind == VALUE_ERROR) {
            return value;
        }
        array_push(arr, value);
    }
        
    return value_arr(arr);
}


//
// Implementations of builtin functions
//

//
// Unary start
//

Value *unary_add(Value *a) {
    if (a->kind == VALUE_INT || a->kind == VALUE_NUMBER || a->kind == VALUE_ARRAY) {
        return a;
    }

    return value_error("unknown target for unary '+'");
}

Value *unary_sub(Value *a) {
    switch (a->kind) {
        case VALUE_INT:    return value_int(a->integer * -1);
        case VALUE_NUMBER: return value_num(a->number * -1);
        case VALUE_ARRAY: {
            Value **arr = NULL;
            For(a->arr) {
                Value *value = unary_sub(a->arr[i]);
                if (value->kind == VALUE_ERROR) {
                    return value;
                }
                array_push(arr, value);
            }
            return value_arr(arr);
        }
        default: {
            fatal("unknown target for unary '-'");
        }
    }

    return NULL;
}

Value *unary_mul(Value *a) {
    switch (a->kind) {
        case VALUE_INT:    return value_int(1);
        case VALUE_NUMBER: return value_num(1);
        case VALUE_ARRAY: {
            Value **arr = NULL;
            For(a->arr) {
               Value *value = unary_mul(a->arr[i]);
               if (value->kind == VALUE_ERROR) {
                   return value;
                }
                array_push(arr, value);
            }

            return value_arr(arr);
        }
        default: {
            fatal("unknown target for unary '-'");
        }
    }

    return NULL;
}

Value *unary_iota(Value *n) {
    if (n->kind != VALUE_INT) {
        return value_error("expected integer");
    }

    Value **arr = NULL;
    for (int i = 0; i < n->integer; i++) {
        array_push(arr, value_int(i + 1));
    }

    return value_arr(arr);
}

Value *unary_random(Value *n) {
    switch (n->kind) {
        case VALUE_INT:    return value_int(rand() % n->integer + 1);
        case VALUE_NUMBER: {
            float rnd = ((float)rand()/(float)(RAND_MAX));
            return value_num(rnd * n->number);
        }
        case VALUE_ARRAY: return array_unary_func(unary_random, n);
        default: return value_error("unexpected value for random");
    }
}

Value *unary_fac(Value *a) {

    assert(a);

    if (a->kind == VALUE_ARRAY) {
    	return array_unary_func(unary_fac, a);
    } else if (a->kind == VALUE_INT) {

        int fac = a->integer;
        int n   = a->integer - 1;
        while (n > 0) {
           fac *= n;
           n--;
        }
        return value_int(fac);
    } else {
        return value_error("unsupported type '%s' for factorial", value_kind_str[a->kind]);
    }
}

Value *unary_rho(Value *arr) {
    assert(arr);

    if (arr->kind != VALUE_ARRAY) {
    	return value_error("rho expected array");
    }

    return value_int(array_len(arr->arr));
}

//
// Unary end
//


//
// Binary start
//

Value *binary_add(Value *a, Value *b) {

    switch (a->kind) {
        case VALUE_INT:    return value_int(a->integer + b->integer);
        case VALUE_NUMBER: return value_num(a->number + b->number);
        case VALUE_ARRAY:  return array_binary_func(binary_add, a, b);
        default:           return value_error("unknown target for addition");
    }
}

Value *binary_sub(Value *a, Value *b) {

    switch (a->kind) {
        case VALUE_INT:    return value_int(a->integer - b->integer);
        case VALUE_NUMBER: return value_num(a->number - b->number);
        case VALUE_ARRAY:  return array_binary_func(binary_sub, a, b);
        default:           return value_error("unknown target for subtraction");
    }
}

Value *binary_mul(Value *a, Value *b) {

    switch (a->kind) {
        case VALUE_INT:    return value_int(a->integer * b->integer);
        case VALUE_NUMBER: return value_num(a->number * b->number);
        case VALUE_ARRAY:  return array_binary_func(binary_mul, a, b);
        default:           return value_error("unknown target for multiplication");
    }
}

Value *binary_div(Value *a, Value *b) {
    Value_Kind target = a->kind;

    if (target == VALUE_INT && b->integer == 0) {
       return value_error("divide by 0");
    }
    if (target == VALUE_NUMBER && b->number == 0.0) {
       return value_error("divide by 0");
    }

    switch (target) {
        case VALUE_INT:    return value_int(a->integer / b->integer);
        case VALUE_NUMBER: return value_num(a->number / b->number);
        case VALUE_ARRAY:  return array_binary_func(binary_div, a, b);
        default:           return value_error("unknown target for division");
    }
}


Value *binary_random(Value *n, Value *upper_bound) {

    if (n->kind != VALUE_INT) {
        return value_error("expected integer arguments for amount, got '%s'", value_kind_str[n->kind]);
    }
    
    Value **result = NULL;
    for (int i = 0; i < n->integer; i++) {
        Value *rnd = unary_random(upper_bound);
        array_push(result, rnd);
    }

    return value_arr(result);
}


Value *binary_min(Value *a, Value *b) {

    assert(a);
    assert(b);

    if (a->kind != b->kind) {
        return value_error("cannot compare different data types");
    }

	switch (a->kind) {
        case VALUE_INT: {
           if (a->integer < b->integer) {
               return value_int(a->integer);
           } else if (a->integer > b->integer) {
               return value_int(b->integer);
           } else {
               return value_int(a->integer);
           }
        }
        case VALUE_NUMBER: {
           if (a->number < b->number) {
               return value_num(a->number);
           } else if (a->number > b->number) {
               return value_num(b->number);
           } else {
               return value_num(a->number);
           }
        }
        case VALUE_ARRAY: return array_binary_func(binary_min, a, b);

        default: return value_error("unexpected value for random");
    }
}

Value *binary_max(Value *a, Value *b) {

    assert(a);
    assert(b);

    if (a->kind != b->kind) {
        return value_error("cannot compare different data types");
    }

	switch (a->kind) {
        case VALUE_INT: {
           if (a->integer > b->integer) {
               return value_int(a->integer);
           } else if (a->integer < b->integer) {
               return value_int(b->integer);
           } else {
               return value_int(a->integer);
           }
        }
        case VALUE_NUMBER: {
           if (a->number > b->number) {
               return value_num(a->number);
           } else if (a->number < b->number) {
               return value_num(b->number);
           } else {
               return value_num(a->number);
           }
        }
        case VALUE_ARRAY: return array_binary_func(binary_max, a, b);

        default: return value_error("unexpected value for random");
    }
}

Value *binary_rho(Value *size, Value *val) {
    
    if (size->kind == VALUE_INT || size->kind == VALUE_NUMBER) {
        Value **arr = NULL;
        
        int amount = 0;
        if (size->kind == VALUE_INT) {
            amount = size->integer;
        } else if (size->kind == VALUE_NUMBER) {
            amount = (int)size->number;
        } else {
            fatal("rho expected number or integer");
        }
        
        if (val->kind == VALUE_INT || val->kind == VALUE_NUMBER) {
            for (int i = 0; i < amount; i++) {
                Value *v = value_copy(val);
                array_push(arr, v);
            }
        } else if (val->kind == VALUE_ARRAY) {
            int total = 0;
            int index = 0;
            while (total < amount) {
                Value *v = value_copy(val->arr[index]);
                array_push(arr, v);
                total++;
                if (index == (array_len(val->arr) - 1)) {
                    index = 0;
                } else {
                    index++;
                }
            }
        } else {
            fatal("rho expected number or integer");
        }

        return value_arr(arr);
        
    } else if (size->kind == VALUE_ARRAY) {
        if (size->arr[0]->kind != VALUE_INT) {
            return value_error("rho expected integer for matrix generation");
        }
        if (array_len(size) == 1) {
            return binary_rho(size->arr[0], val);
        }
        
        Value **mat = NULL;
        int rows = size->arr[0]->integer;
        int cols = size->arr[1]->integer;
        for (int i = 0; i < rows; i++) {
            Value *res = binary_rho(size->arr[1], val);
            for (int j = 0; j < cols; j++) {
                array_push(mat, res->arr[j]);
            }
            
        }
        return value_matrix(mat, rows, cols);
    }
        
    
        
    return value_error("rho expected integer or array on the left-hand side");
}

Value *binary_equal(Value *a, Value *b) {

    switch (a->kind) {
        case VALUE_INT: {
            return value_int(value_equal(a, b) ? 1 : 0);
        }
        case VALUE_NUMBER: {
            return value_num(value_equal(a, b) ? 1.0 : 0.0);
        }
        case VALUE_ARRAY: {
            return array_binary_func(binary_equal, a, b);
        }
        default: {
            fatal("unimplemented");
        }
    }

    return NULL;
}

Value *binary_power(Value *x, Value *p) {

    switch (x->kind) {
        case VALUE_INT: {
            double result = pow((double)x->integer, (double)p->integer);
            return value_int((int)result);
        }
        case VALUE_NUMBER: {
            double result = pow(x->number, x->number);
            return value_num(result);
        }
        case VALUE_ARRAY: {
            return array_binary_func(binary_power, x, p);
        }
        default: {
            fatal("unimplemented");
        }
    }

    return value_error("power expected integer, got '%s'", value_kind_str[p->kind]);
}

//
// Binary end
//


//
// Evaluation implementation
//

Value *eval_function_call(Function *f) {
    
    assert(f);
    
    switch (f->kind) {
        case FUNC_UNARY: {
            Value *val = eval_expr(f->arg_right);
            if (val->kind == VALUE_ERROR) {
                return val;
            }
    		func_scope_enter();
            Value **vals = NULL;
            For(f->stmts) {
                Value *val = eval_decl(f->stmts[i]);
                if (val->kind == VALUE_ERROR) {
                    func_scope_exit();
                    return val;
                }
                array_push(vals, val);
            }
			func_scope_exit();
            return vals[array_len(vals) - 1];
        }
        case FUNC_BINARY: {
            Value *left = eval_expr(f->arg_left);
            if (left->kind == VALUE_ERROR) {
                return left;
            }
            Value *right = eval_expr(f->arg_right);
            if (right->kind == VALUE_ERROR) {
                return right;
            }
            func_scope_enter();
            Value **vals = NULL;
            For(f->stmts) {
                Value *val = eval_decl(f->stmts[i]);
                if (val->kind == VALUE_ERROR) {
                    func_scope_exit();
                    return val;
                }
                array_push(vals, val);
            }
            func_scope_exit();
            return vals[array_len(vals) - 1];
        }
        default: {
            fatal("unknown function kind");
        }
    }

    return NULL;
}


Value *eval_operand(Ast *operand) {

    switch (operand->kind) {
        case AST_INT: {
            return value_int(operand->integer);
        };
        case AST_NUMBER: {
            return value_num(operand->number);
        };
        case AST_ARRAY: {
            Value **vals = NULL;
            For(operand->arr) {
                Value *val = eval_operand(operand->arr[i]);
                array_push(vals, val);
            }
            return value_arr(vals);
        };
        case AST_SYMBOL: {
            Value *val = sym_table_lookup(operand->symbol);
            if (!val) {
                return value_error("undeclared symbol '%s'", operand->symbol);
            }
            return val;
        };
        default: {
            fatal("unknown expression type");
        };
    }

    return NULL;
}

Value *eval_unary(Ast *expr) {
    assert(expr->kind == AST_EXPR_UNARY && "not expr expr");

    Value *right = eval_expr(expr->unary.expr);
    if (right->kind == VALUE_ERROR) {
        return right;
    }

    if (expr->unary.func->kind == AST_BUILTIN) {
        //
        // Operator case
        //
        //  such as: '+/ 1 2 3 4' where '/' is the operator
        //
        if (expr->unary.op != NULL) {
            //
            // @Cleanup
            //
            // Don't compare strings here since we have the
            // token_type
            //
            // -fbl 16-10-19
            //
            if (str_eq("/", expr->unary.op)) {
                Func_Entry func = func_lookup(expr->unary.func->builtin);
                if (!func.binary) {
                    return value_error("undefined binary function '%s'", token_type_names[expr->unary.func->builtin]);
                }
                Value *zero = value_int(func.acc);

                return array_reduce(func.binary, zero, right);
            } else if (str_eq("map", expr->unary.op)) {
                Func_Entry func = func_lookup(expr->unary.func->builtin);
                if (!func.unary) {
                    return value_error("undefined unary function '%s'", token_type_names[expr->unary.func->builtin]);
                }

                return array_unary_func(func.unary, right);
            } else {
                return value_error("unknown array operator %s", expr->unary.op);
            }
       }

       //
       // Normal unary function
       //
       Func_Entry func = func_lookup(expr->unary.func->builtin);
       if (!func.unary) {
           return value_error("undefined unary function '%s'", expr->unary.func);
       }

	   return func.unary(right);

    } else {

        Function *func = func_table_lookup(expr->unary.func->symbol);
        if (!func) {
            return value_error("undefined function '%s'", expr->unary.func->symbol);
        }

        if (expr->unary.op != NULL) {
            return value_error("operators not supported for user defined functions");
        }

        Value *arg_right = value_symbol(func->arg_right->symbol);

        sym_table_bind(arg_right, right);

        Value *result = eval_function_call(func);

        // reset stack
        sym_table_exit(1);

        return result;
    }
}

Value *eval_expr(Ast *expr) {
    switch (expr->kind) {
        case AST_EXPR_BINARY: return eval_binary(expr);
        case AST_EXPR_UNARY:  return eval_unary(expr);
        default:              return eval_operand(expr);
    }
}

Value *eval_binary(Ast *expr) {
    assert(expr->kind == AST_EXPR_BINARY && "not binary value");

    Value *left = eval_operand(expr->binary.expr_left);
    if (left->kind == VALUE_ERROR) {
        return left;
    }
    Value *right = eval_expr(expr->binary.expr_right);
    if (right->kind == VALUE_ERROR) {
        return right;
    }

    if (expr->binary.func->kind == AST_BUILTIN) {
        Func_Entry func = func_lookup(expr->binary.func->builtin);
        if (!func.binary) {
            return value_error("undefined unary function '%s'", expr->binary.func);
        }
        
        if (func.should_promote) {
            value_promote_par(left, right);
        }
        
        return func.binary(left, right);
    } else {
        Function *func = func_table_lookup(expr->binary.func->symbol);
        if (!func) {
            return value_error("undefined function '%s'", expr->binary.func->symbol);
        }

        Value *arg_left  = value_symbol(func->arg_left->symbol);
        Value *arg_right = value_symbol(func->arg_right->symbol);

        sym_table_bind(arg_left, left);
        sym_table_bind(arg_right, right);

        Value *result = eval_function_call(func);

        // reset stack
        sym_table_exit(2);

        return result;
    }

}

Value *eval_stmt(Ast *stmt) {
    switch (stmt->kind) {
        case AST_STMT_EXPR:  return eval_expr(stmt->expr_stmt.expr);
        case AST_STMT_PRINT: {
            Value *val = eval_expr(stmt->expr_stmt.expr);
            if (val->kind == VALUE_ERROR) {
                return NULL;
            }
            value_print(val);
            printf("\n");
            return val;
        }
        default: fatal("undefined stmt type");
    }
    return NULL;
}

Value *eval_decl(Ast *decl) {
    switch (decl->kind) {
        case AST_DECL_VAR: {
            Value *symbol = value_symbol(decl->var.symbol->symbol);
            Value *val = eval_expr(decl->var.expr);

            sym_table_bind(symbol, val);

            return symbol;
        }
        case AST_DECL_STMT: {
            return eval_stmt(decl->stmt.stmt);
        }
        case AST_DECL_FUNC: {
            Function *func = function_new(decl->func.func_name->symbol, decl->func.arg_left, decl->func.arg_right, decl->func.block->block.decls);
            func_table_bind(func);
			assert(func_table_lookup(func->name) != NULL);
            return value_func(func->name);
        }
        default: {
            fatal("undefined decl type");
        }
    }
    return NULL;
}



