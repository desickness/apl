
Function *function_new(const char *name, Ast *arg_left, Ast *arg_right, Ast **stmts) {

    Function *f = xmalloc(sizeof(Function));

    f->kind = arg_left ? FUNC_BINARY : FUNC_UNARY;
    f->name = strdup(name);

    f->arg_left  = arg_left;
    f->arg_right = arg_right;

    f->stmts = stmts;
    return f;
}

void sym_table_bind(Value *symbol, Value *val) {

    assert(interp.sym_table.top < (INTERP_SYM_TABLE_SIZE - 1) && "stack overflow");

    interp.sym_table.stack[interp.sym_table.top].symbol = symbol;
    interp.sym_table.stack[interp.sym_table.top].val = val;
    interp.sym_table.top++;
}

Value *sym_table_lookup(const char *name) {

    for (int i = (interp.sym_table.top - 1); i >= 0; i--) {
        if (str_eq(name, interp.sym_table.stack[i].symbol->symbol)) {
            return interp.sym_table.stack[i].val;
        }
    }

    return NULL;
}

void sym_table_exit(int num_args) {
    interp.sym_table.top -= num_args;
}

void sym_table_print(Sym_Table *st) {
    printf("size    : %zu\n", st->top);
    printf("content :\n");
    for (int i = (st->top - 1); i >= 0; i--) {
        printf("%s: ", st->stack[i].symbol->symbol);
        value_print(st->stack[i].val);
        printf("\n");
    }
}


//
// Simple Function table implementation
//
void func_table_init() {
    interp.func_table.top = 0;
}

void func_table_bind(Function *func) {

    assert(interp.func_table.top < (INTERP_SYM_TABLE_SIZE - 1) && "function table stack overflow");

    interp.func_table.stack[interp.func_table.top] = func;
    interp.func_table.top++;
}

Function *func_table_lookup(const char *name) {
    assert(name);

    for (int i = (interp.func_table.top - 1); i >= 0; i--) {
        //
        // Check for scope marker
        //
        if (interp.func_table.stack[i] == NULL) {
        	continue;
        }
        if (str_eq(name, interp.func_table.stack[i]->name)) {
            return interp.func_table.stack[i];
        }
    }

    return NULL;
}

void func_debug() {
	for (int i = (interp.func_table.top - 1); i >= 0; i--) {
        //
        // Check for scope marker
        //
        if (interp.func_table.stack[i] == NULL) {
            printf("NULL\n");
            continue;
        }
        printf("%s\n", interp.func_table.stack[i]->name);
    }
}

void func_scope_enter() {
    //
    // Use NULL as scope marker
    //
	interp.func_table.stack[interp.func_table.top] = NULL;
    interp.func_table.top++;
}

void func_scope_exit() {
    do {
    	interp.func_table.top--;
    } while (interp.func_table.stack[interp.func_table.top] != NULL);
}

//
// Func table for builtin functions
//
// Most builtin functions in APL have a binary and unary version.
// We can simply lookup by token type, since the token type for
// functions are reserved keywords.
//

Func_Entry func_lookup(Token_Type t) {
    return builtin_func_table[t];
}

