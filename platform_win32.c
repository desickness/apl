
static bool disable_colors = false;

void text_red(const char *str) {
    if (disable_colors) {
        printf("%s", str);
    } else {
        printf("%s", str);
    }
}

void text_green(const char *str) {
    if (disable_colors) {
        printf("%s", str);
    } else {
        printf("%s", str);
    }
}