#!/bin/bash

code="$PWD"
opts=-g
mkdir -p build
cd build > /dev/null
cc $opts $code/apl.c -o apl -std=c99
cd $code > /dev/null
