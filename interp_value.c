
bool value_kind_equal(Value *a, Value *b) {
    assert(a);
    assert(b);

    return a->kind == b->kind;
}

Value *value_new(Value_Kind kind) {
    Value *val = xmalloc(sizeof(Value));
    val->kind = kind;

    return val;
}

Value *value_int(int i) {
    Value *val = value_new(VALUE_INT);
    val->integer = i;

    return val;
}

Value *value_num(double num) {
    Value *val = value_new(VALUE_NUMBER);
    val->number = num;

    return val;
}

Value *value_symbol(const char *symbol) {
    assert(symbol);

    Value *value = value_new(VALUE_SYMBOL);
    value->kind = VALUE_SYMBOL;
    int len = strlen(symbol);
    value->symbol = xmalloc(len + 1);
    strcpy(value->symbol, symbol);
    value->symbol[len] = '\0';

    return value;
}


Value *value_arr(Value **arr) {
    assert(arr);

    Value *val = value_new(VALUE_ARRAY);
    val->arr = arr;

    return val;
}

Value *value_matrix(Value **mat, int rows, int cols) {
    assert(mat);
    assert(((array_len(mat)) == (rows * cols)));

    Value *val = value_new(VALUE_MATRIX);
    
    val->matrix.mat  = mat;
    val->matrix.rows = rows;
    val->matrix.cols = cols;

    return val;
}


Value_Kind value_target(Value *a, Value *b) {
	return a->kind > b->kind ? a->kind : b->kind;
}


Value *value_error(const char *fmt, ...) {
    Value *value = value_new(VALUE_ERROR);
    value->kind = VALUE_ERROR;

    va_list args;
    va_start(args, fmt);
    size_t n = 1 + vsnprintf(NULL, 0, fmt, args);
    va_end(args);
    char *str = xmalloc(n);
    va_start(args, fmt);
    vsnprintf(str, n, fmt, args);
    va_end(args);

    value->error = strdup(str);

    return value;
}

Value *value_func(const char *symbol) {
    Value *value  = value_new(VALUE_FUNC);
    value->kind   = VALUE_FUNC;
    value->symbol = strdup(symbol);

    return value;
}

Value *value_promote(Value *value, Value_Kind target);

Value *value_promote_int(Value *i, Value_Kind target) {
	assert(i->kind == VALUE_INT);

    switch (target) {
        case VALUE_INT:    return value_int(i->integer);
        case VALUE_NUMBER: return value_num((double)i->integer);
        case VALUE_ARRAY:  {
            Value **arr = NULL;
            array_push(arr, value_int(i->integer));
            return value_arr(arr);
        }
        case VALUE_MATRIX: {
            return i;
        }
        default : {
            fatal("cannot promote type '%s'", value_kind_str[i->kind]);
        }
    }
    
    return NULL;
}

Value *value_promote_num(Value *num, Value_Kind target) {
	assert(num->kind == VALUE_NUMBER);
    
    switch (target) {
        case VALUE_NUMBER: return value_num(num->number);
        case VALUE_ARRAY:  {
            Value **arr = NULL;
            array_push(arr, value_num(num->number));
            return value_arr(arr);
        }
        case VALUE_MATRIX: {
            return num;
        }
        default : {
            fatal("cannot promote type '%s'", value_kind_str[num->kind]);
        }
    }
    
    return NULL;
}

Value *value_promote_array(Value *arr, Value_Kind target) {
	assert(arr->kind == VALUE_ARRAY);
    
    switch (target) {
        case VALUE_ARRAY:  {
            return value_arr(arr->arr);
        }
        case VALUE_MATRIX: {
            return arr;
        }
        default : {
            fatal("cannot promote type '%s'", value_kind_str[arr->kind]);
        }
    }
    
    return NULL;
}

Value *value_promote(Value *value, Value_Kind target) {
     
    if (value->kind == target) {
        return value;
    }
    
    if (value->kind > target) {
        return value_error("cannot promote downwards from '%s' to '%s'", value_kind_str[value->kind], value_kind_str[target]);
    }
    
    switch (value->kind) {
        case VALUE_INT:    return value_promote_int(value, target);
        case VALUE_NUMBER: return value_promote_num(value, target);
        case VALUE_ARRAY:  return value_promote_array(value, target);
        case VALUE_MATRIX: {
            return value;
        }
        default : {
            fatal("cannot promote type '%s'", value_kind_str[value->kind]);
        }
    }
    
    return NULL;
}

void value_print(Value *v);

void value_promote_par(Value *a, Value *b) {
    
    Value_Kind target = value_target(a, b);
    
    *a = *value_promote(a, target);
    *b = *value_promote(b, target);
    
    if (target == VALUE_ARRAY) {
        if (a->arr[0]->kind == b->arr[0]->kind) {
            return;
        } 
        
        Value_Kind array_target = value_target(a->arr[0], b->arr[0]);
        
        Value **a_arr = NULL;
        For (a->arr) {
            Value *val = value_promote(a->arr[i], array_target);
            array_push(a_arr, val);
        }
        *a = *value_arr(a_arr);
        Value **b_arr = NULL;
        For (b->arr) {
            Value *val = value_promote(b->arr[i], array_target);
            array_push(b_arr, val);
        }
        *b = *value_arr(b_arr);
    }
}

Value *value_copy(Value *val) {
    assert(val);

    switch (val->kind) {
        case VALUE_INT:    return value_int(val->integer);
        case VALUE_NUMBER: return value_num(val->number);
        case VALUE_SYMBOL: return value_symbol(val->symbol);
        default: {
            fatal("unimplemented type");
        }
    }
    return NULL;
}

bool value_equal(Value *a, Value *b) {
    assert(a);
    assert(b);

    if (a->kind != b->kind) {
        return false;
    }

    switch (a->kind) {
        case VALUE_INT:    return a->integer == b->integer;
        case VALUE_NUMBER: return a->number == b->number;
        case VALUE_ARRAY: {
            if (array_len(a->arr) != array_len(b->arr)) {
                return false;
            }
            For (b->arr) {
                if (value_equal(a->arr[i], b->arr[i]) != true) {
                    return false;
                }
            }
            return true;
        }
        default: {
            return false;
        }
    }

    return false;
}

char *value_str(Value *val) {
    assert(val);

    char *message = NULL; // array
    switch (val->kind) {
        case VALUE_INT:    array_printf(message, "%d", val->integer); break;
        case VALUE_NUMBER: array_printf(message, "%f", val->number);  break;
        case VALUE_SYMBOL: sprintf(message, "%s", val->symbol);       break;
        case VALUE_ERROR:  sprintf(message, "%s", val->error);        break;
        case VALUE_ARRAY: {
            array_printf(message, "[");
            For (val->arr) {
                if (i != 0) {
                    array_printf(message, " ");
                }
                char *str = value_str(val->arr[i]);
                array_printf(message, str);
            }
            array_printf(message, "]");
            break;
        }
        case VALUE_MATRIX: {
            for (int i = 0; i < val->matrix.rows; i++) {
                array_printf(message, "[");
                for (int j = 0; j < val->matrix.cols; j++) {
                    if (i != 0) {
                        array_printf(message, " ");
                    }
                    char *str = value_str(val->matrix.mat[j]);
                    array_printf(message, str);
                }
                array_printf(message, "]\n");
            }
            break;
        }

        default: {
            fatal("unknown value type");
        };
    }

    return message;
}

void value_print(Value *val) {
    assert(val);

    switch (val->kind) {
        case VALUE_INT:    printf("%d", val->integer); break;
        case VALUE_NUMBER: printf("%f", val->number);  break;
        case VALUE_SYMBOL: printf("%s", val->symbol);  break;
        case VALUE_ERROR:  printf("%s", val->error);   break;
        case VALUE_FUNC:   printf("%s", val->symbol);  break;
        case VALUE_ARRAY: {
            printf("[");
            For (val->arr) {
                if (i != 0) {
                    printf(" ");
                }
                value_print(val->arr[i]);
            }
            printf("]");
            break;
        }
        case VALUE_MATRIX: {
            for (int i = 0; i < val->matrix.rows; i++) {
                if (i != 0) {
                    printf("\n");
                }
                printf("[");
                for (int j = 0; j < val->matrix.cols; j++) {
                    if (j != 0) {
                        printf(" ");
                    }
                    value_print(val->matrix.mat[j]);
                }
                printf("]");
            }
            break;
        }
        default: {
            fatal("unknown value type");
        };
    }
};