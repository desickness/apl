
#define MIN(x, y) ((x) <= (y) ? (x) : (y))
#define MAX(x, y) ((x) >= (y) ? (x) : (y))
#define CLAMP_MAX(x, max) MIN(x, max)
#define CLAMP_MIN(x, min) MAX(x, min)
#define IS_POW2(x) (((x) != 0) && ((x) & ((x)-1)) == 0)

void *xrealloc(void *ptr, size_t num_bytes) {
    ptr = realloc(ptr, num_bytes);
    if (!ptr) {
        perror("xrealloc failed");
        exit(1);
    }
    return ptr;
}

void *xmalloc(size_t num_bytes) {
    void *ptr = malloc(num_bytes);
    if (!ptr) {
        perror("xmalloc failed");
        exit(1);
    }
    return ptr;
}

char *strf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    size_t n = 1 + vsnprintf(NULL, 0, fmt, args);
    va_end(args);
    char *str = xmalloc(n);
    va_start(args, fmt);
    vsnprintf(str, n, fmt, args);
    va_end(args);
    return str;
}

//
// Stretchy buffers/arrays taken from the bitwise project on github 
// that were invented (?) by Sean Barrett the author of the stb libraries also on github
//
// They enable dynamic array like behavior, and is a giant quality of 
// life improvement.
// 
// fbl. January 20, 2019
//


typedef struct Array_Header {
    size_t len;
    size_t cap;
    char arr[];
} Array_Header;

#define array__hdr(b) ((Array_Header *)((char *)(b) - offsetof(Array_Header, arr)))

#define array_len(b) ((b) ? array__hdr(b)->len : 0)
#define array_cap(b) ((b) ? array__hdr(b)->cap : 0)
#define array_end(b) ((b) + array_len(b))
#define array_sizeof(b) ((b) ? array_len(b)*sizeof(*b) : 0)

#define array_free(b) ((b) ? (free(array__hdr(b)), (b) = NULL) : 0)
#define array_fit(b, n) ((n) <= array_cap(b) ? 0 : ((b) = array__grow((b), (n), sizeof(*(b)))))
#define array_push(b, ...) (array_fit((b), 1 + array_len(b)), (b)[array__hdr(b)->len++] = (__VA_ARGS__))
#define array_printf(b, ...) ((b) = array__printf((b), __VA_ARGS__))
#define array_clear(b) ((b) ? array__hdr(b)->len = 0 : 0)

void *array__grow(const void *arr, size_t new_len, size_t elem_size) {
    assert(array_cap(arr) <= (SIZE_MAX - 1)/2);
    size_t new_cap = CLAMP_MIN(2*array_cap(arr), MAX(new_len, 16));
    assert(new_len <= new_cap);
    assert(new_cap <= (SIZE_MAX - offsetof(Array_Header, arr))/elem_size);
    size_t new_size = offsetof(Array_Header, arr) + new_cap*elem_size;
    Array_Header *new_hdr;
    if (arr) {
        new_hdr = xrealloc(array__hdr(arr), new_size);
    } else {
        new_hdr = xmalloc(new_size);
        new_hdr->len = 0;
    }
    new_hdr->cap = new_cap;
    return new_hdr->arr;
}

char *array__printf(char *arr, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    size_t cap = array_cap(arr) - array_len(arr);
    size_t n = 1 + vsnprintf(array_end(arr), cap, fmt, args);
    va_end(args);
    if (n > cap) {
        array_fit(arr, n + array_len(arr));
        va_start(args, fmt);
        size_t new_cap = array_cap(arr) - array_len(arr);
        n = 1 + vsnprintf(array_end(arr), new_cap, fmt, args);
        assert(n <= new_cap);
        va_end(args);
    }
    array__hdr(arr)->len += n - 1;
    return arr;
}


#define For(arr) for (int i = 0; i < array_len(arr); i++)


// Error handling

static void fatal(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    printf("\n");
    va_end(args);
    exit(1);
}


// File IO



char *file_read(const char *path) {
    FILE *file = fopen(path, "rb");
    
    if (!file) {
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    long len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char *buf = xmalloc(len + 1);
    if (len && fread(buf, len, 1, file) != 1) {
        fclose(file);
        free(buf);
        return NULL;
    }
    fclose(file);   
    buf[len] = 0;
    return buf;
}

enum {
    LINE_LENGTH = 1024
};

char **file_read_lines(const char *path) {
    
    FILE *file = fopen(path, "rb");
    if (!file) {
        return NULL;
    }
    
    char **lines = NULL; // array
    char line[LINE_LENGTH];
    while(fgets(line, LINE_LENGTH, file)) {
        array_push(lines, strf("%s", line));
    }
    
    fclose(file);
    return lines;
}

bool str_eq(const char *a, const char *b) {
    return strcmp(a, b) == 0;
}