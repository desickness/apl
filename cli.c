

typedef enum {
    FLAG_INT,
    FLAG_BOOL,
    FLAG_STR
} Flag_Kind;

static char* flag_name[] = {
    [FLAG_INT]  = "int",
    [FLAG_BOOL] = "bool",
    [FLAG_STR]  = "string",
};


typedef struct {
    Flag_Kind kind;
    const char *name;
    const char *help;
    union {
        int  *i;
        bool *b;
        char **str;
    };
} Flag;

static Flag *flags = NULL; // array
static int flag_indent_width = 4;
static int flag_longest_name = 0;
static char *flag_program_name = NULL;

Flag *flag_lookup(const char *name) {
    for (size_t i = 0; i < array_len(flags); i++) {
        if (str_eq(flags[i].name, name)) {
            return &flags[i];    
        }
    }
    return NULL;
}

static void flag_usage() {
    if (!flags) {
        printf("no flags defined");
        return;
    }
    
    printf("\n");
    printf("Usage: "); 
    if (flag_program_name) {
        printf("%s ", flag_program_name);       
    }
    printf("[options]\n");
    for (int i = 0; i < array_len(flags); i++) {
        printf("%*c", flag_indent_width, ' ');
        printf("%-*s%s\n", (flag_longest_name + 3), flags[i].name, flags[i].help);
    }
    printf("\n");
}

static void flag_set_longest(const char *name) {
    int len = strlen(name);
    flag_longest_name = flag_longest_name > len 
        ? flag_longest_name
        : len;
}

static void flag_int(int *i, const char *name, const char *help) {
    Flag flag = {0};
    flag_set_longest(name);
    flag.name = name;
    flag.help = help;
    flag.kind = FLAG_INT;
    flag.i = i;
    array_push(flags, flag);
}

static void flag_bool(bool *b, const char *name, const char *help) {
    Flag flag = {0};
    flag_set_longest(name);
    flag.name = name;
    flag.help = help;
    flag.kind = FLAG_BOOL;
    flag.b = b;
    array_push(flags, flag);
}

static void flag_str(char **str, const char *name, const char *help) {
    Flag flag = {0};
    flag_set_longest(name);
    flag.name = name;
    flag.help = help;
    flag.kind = FLAG_STR;
    flag.str  = str;
    array_push(flags, flag);
}

static void flag_parse(int *argc_ptr, char ***argv_ptr) {
    int argc = *argc_ptr;
    char ** argv = *argv_ptr;
    flag_program_name = 2 + argv[0];
    if (argc <= 1) {
        return;
    }
    
    for (int i = 1; i < argc; i++) {
        Flag *flag = flag_lookup(argv[i]);
        if (!flag) {
            printf("unknown flag '%s'\n", argv[i]);
            continue;
        }
        
        switch (flag->kind) {
            case FLAG_BOOL: {
                *flag->b = true;
                break;
            }
            case FLAG_INT: {
                if (i + 1 < argc) {
                    i++;
                    *flag->i = atoi(argv[i]);
                }
                break;
            } 
            case FLAG_STR: {
                if (i + 1 < argc) {
                    i++;
                    *flag->str = argv[i];
                }
                break;
            }
        }
    }
}